//
//  UIView+RDRTheme.h
//  Radar
//
//  Created by Rollin Su on 2/24/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RDRTheme)

+ (UIColor *)rdr_primaryThemeColor;
- (void)rdr_applyButtonTheme;
- (void)rdr_applyButtonLightTheme;
- (void)rdr_applyInactiveButtonTheme;
- (void)rdr_applyMainBackgroundColor;
- (void)rdr_applyShadow;
@end
