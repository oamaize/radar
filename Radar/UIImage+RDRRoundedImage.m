//
//  UIImage+RDRRoundedImage.m
//  Radar
//
//  Created by Ota-Benga Amaize on 1/20/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "UIImage+RDRRoundedImage.h"

@implementation UIImage (RDRRoundedImage)

+ (UIImage*)rdr_roundedImage:(UIImage *)image
{
    
    //UIImage *image = [UIImage imageNamed:filename];
    CGFloat width = MIN(image.size.width, image.size.height);
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, width, width);
    imageLayer.contents = (id) image.CGImage;
    imageLayer.contentsGravity = kCAGravityCenter;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = width * 0.5f;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, width));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}

@end
