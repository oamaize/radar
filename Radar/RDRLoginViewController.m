//
//  RDRLoginViewController.m
//  Radar
//
//  Created by Rollin Su on 11/11/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRLoginViewController.h"
#import "RDRPhotoCaptureViewController.h"
#import "RDRSmsVerifyViewController.h"

@interface RDRLoginViewController () <RDRPhotoCaptureViewControllerDelegate, RDRSmsVerifyViewControllerDelegate, UIPageViewControllerDelegate, UIPageViewControllerDataSource>
@property (nonatomic, strong) UIPageViewController *pageViewController;
@end

@implementation RDRLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    [self.pageViewController setViewControllers:@[[[RDRPhotoCaptureViewController alloc] initWithNibName:@"RDRPhotoCaptureViewController" bundle:nil]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    self.pageViewController.view.frame = self.view.bounds;
    
    [self.pageViewController willMoveToParentViewController:self];
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onLoginButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSignUpButtonTapped:(id)sender {
    //[self dismissViewControllerAnimated:YES completion:nil];
    RDRPhotoCaptureViewController *photoCaptureVC = [[RDRPhotoCaptureViewController alloc] initWithNibName:@"RDRPhotoCaptureViewController" bundle:nil];
    photoCaptureVC.delegate = self;
    [self presentViewController:photoCaptureVC animated:YES completion:nil];
}

- (IBAction)onSMSButtonTapped:(id)sender{
    
    RDRSmsVerifyViewController *smsVerifyVC = [[RDRSmsVerifyViewController alloc]initWithNibName:@"RDRSmsVerifyViewController" bundle:nil];
    smsVerifyVC.delegate = self;
    [self presentViewController:smsVerifyVC animated:YES completion:nil];
}

#pragma mark - RDRPhotoCaptureViewControllerDelegate
- (void)photoCaptureViewControllerDidCancel:(RDRPhotoCaptureViewController *)photoCaptureViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)photoCaptureViewControllerDidCapturePhoto:(RDRPhotoCaptureViewController *)photoCaptureViewController
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self dismissViewControllerAnimated:YES completion:nil];
    }];

}

@end
