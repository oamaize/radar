//
//  UIView+RDRRoundedCorners.h
//  Radar
//
//  Created by Rollin Su on 3/23/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RDRRoundedCorners)

- (void)rdr_roundCorners;
- (void)rdr_roundCornersWithRadius:(CGFloat)radius;
- (void)rdr_roundCorners:(UIRectCorner)corners radius:(CGFloat)radius;
@end
