//
//  RDRMainViewController.m
//  Radar
//
//  Created by Rollin Su on 11/13/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRMainViewController.h"
#import "RDRMainThumbnailCell.h"
#import "TransferService_TransferService.h"
#import "RDRUser.h"
#import "RDRMenuViewController.h"
#import "RDRSlideUpViewController.h"
#import "RDRPopUpViewController.h"
#import "RDRRadarAppDelegate.h"
#import "RDRChatLogs.h"
#import "RDRChatMessage.h"
#import "RDRMainHeaderView.h"
#import "RDRGradientActivityIndicatorView.h"
#import "RDRSendChatViewController.h"
#import "RDRSignUpViewController.h"
#import "RDRConnectDeviceViewController.h"
#import "UIView+RDRTheme.h"
#import "RDRFakeMessasgeSender.h"
#import "RDRChatViewController.h"
#import "RDRChatImageViewController.h"
#import "RDRLocalUserFinderViewController.h"
#import "RDRAddressBookController.h"
#import "RDRDoNotDisturbViewController.h"
#import "RDRConnectDeviceViewController.h"

#import <JSBadgeView/JSBadgeView.h>
#import <Parse/Parse.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/AFNetworking.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import <UIAlertView-Blocks/UIAlertView+Blocks.h>

#define RDRIsUsingDummyData [[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyUseDummyData]

@interface RDRMainViewController () <UICollectionViewDataSource, UICollectionViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate, RDRSendChatViewControllerDelegate, UITextFieldDelegate>
@property (nonatomic, strong) NSArray *imagesFeed;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSString *cacheDirectoryPath;
@property (strong, nonatomic) NSMutableArray *thumbnails;
@property (strong, nonatomic) NSMutableDictionary *dummyUsers;
@property (strong, nonatomic) NSMutableArray *filteredUsers;
@property (strong, nonatomic) NSMutableArray *radarFriends;
@property (strong, nonatomic) NSMutableArray *localFriends;
@property (strong, nonatomic) RDRMainHeaderView *sectionHeaderView;
@property (strong, nonatomic) RDRMainHeaderView *localHeaderView;
@property (strong, nonatomic) RDRFakeMessasgeSender *messageSender;
@property (strong, nonatomic) NSMutableDictionary *incomingMessages;
@property (nonatomic, assign, readwrite) RDRMainViewControllerType type;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) RDRLocalUserFinderViewController *localFinderViewController;
@property (assign, nonatomic) CLLocationDegrees currentLongitude;
@property (assign, nonatomic) CLLocationDegrees currentLatitude;
@property (assign, nonatomic) BOOL isReadyForLocationUpdates;
@property (assign, nonatomic) NSInteger selectedGender;
@property (strong, nonatomic) NSMutableDictionary *prefetchedMessages;
@property (strong, nonatomic) NSMutableDictionary *prefetchedImageMessages;

// Spinner, scanner
@property (weak, nonatomic) IBOutlet UIView *spinnerView;
@property (strong, nonatomic) UIView *scanningView;
@property (weak, nonatomic) UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIImageView *scannerImageView;


@property (assign, nonatomic) BOOL needLocalFirstTimeRefresh;
@property (assign, nonatomic) CGFloat lastContentOffsetY;
@property (assign, nonatomic) NSInteger lastScrollDirection;
@property (strong, nonatomic) UIView *noUsersFoundView;
@property (strong, nonatomic) UIView *noFriendsFoundView;


@property (strong, nonatomic) UIViewController *dndViewController;

@property (assign, nonatomic) BOOL isReloading;
@end

@implementation RDRMainViewController

- (instancetype) initWithType:(RDRMainViewControllerType)type
{
    // Not the most elegant, but whatever... reassigning self
    self = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"mainViewController"];
    if (self) {
        _type = type;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Blur statusbar
    //http://stackoverflow.com/questions/18199763/ios-7-status-bar-transparent
    //http://stackoverflow.com/questions/19065098/status-bar-text-color-ios-7
    CGRect statusBarFrame = [self.view convertRect: [UIApplication sharedApplication].statusBarFrame fromView: nil];
    UIToolbar *statusBarBackground = [[UIToolbar alloc] initWithFrame: statusBarFrame];
    [statusBarBackground setTranslucent:NO];
    [statusBarBackground setBarTintColor:[UIColor colorWithRed:62/255.0 green:135/255.0 blue:207/255.0 alpha:1.f]];
    statusBarBackground.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview: statusBarBackground];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"RDRMainThumbnailCell" bundle:nil] forCellWithReuseIdentifier:@"thumbCell"];
    
    
    // Get the file cache directory
    self.cacheDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    self.collectionView.alwaysBounceVertical = YES;
    
    
    self.radarFriends = [NSMutableArray new];
    self.localFriends = [NSMutableArray new];
    
    if (![self loadMessages]) {
        self.incomingMessages = [NSMutableDictionary new];
    }
    self.prefetchedMessages = [NSMutableDictionary new];
    self.prefetchedImageMessages = [NSMutableDictionary new];
    
    [(UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout setSectionInset:UIEdgeInsetsMake(44.f, 0.f, 0.f, 0.f)];
    
    if (self.type == RDRMainViewControllerTypeLocal) {
        self.localHeaderView = [[RDRMainHeaderView alloc] initWithFrame:CGRectMake(0.f, 0.f, self.collectionView.bounds.size.width, 44.f) type:RDRMainHeaderViewTypeLocal];
        [self.collectionView addSubview:self.localHeaderView];
        [self.localHeaderView.settingsButton addTarget:self action:@selector(onSettingsButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.localHeaderView.refreshButton addTarget:self action:@selector(onRefreshButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.localHeaderView.filterControl addTarget:self action:@selector(onGenderFilterValueChanged:) forControlEvents:UIControlEventValueChanged];
        
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:13.f];
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                               forKey:NSFontAttributeName];
        [self.localHeaderView.filterControl setTitleTextAttributes:attributes
                                        forState:UIControlStateNormal];
        
        self.scanningView = [[NSBundle mainBundle] loadNibNamed:@"RDRScanningView" owner:self options:nil][0];
        [self.view addSubview:self.scanningView];
        self.scanningView.hidden = YES;
        self.loadingView = self.scanningView;
        
        self.dndViewController = [RDRDoNotDisturbViewController new];
    } else if(self.type == RDRMainViewControllerTypeFriends) {
        
        self.sectionHeaderView = [[RDRMainHeaderView alloc] initWithFrame:CGRectMake(0.f, 0.f, self.collectionView.bounds.size.width, 44.f) type:RDRMainHeaderViewTypeFriends];
        [self.collectionView addSubview:self.sectionHeaderView];
        [self.sectionHeaderView.shareButton addTarget:self action:@selector(onShareButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.sectionHeaderView.settingsButton addTarget:self action:@selector(onSettingsButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        self.sectionHeaderView.searchBar.delegate = self;
        self.loadingView = self.spinnerView;
    }
    
    // Hide toolbar in beginning
    //[self.collectionView setContentOffset:CGPointMake(0.f, 44.f) animated:NO];
    
    
    //add observer for message push
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageReceivedNotification:) name:RDRNotificationIncomingMessage object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onApplicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendsListUpdated:) name:RDRNotificationFriendsListUpdated object:nil];
    
    if (self.type == RDRMainViewControllerTypeLocal) {
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) {
            self.localFinderViewController = [[RDRLocalUserFinderViewController alloc] initWithLocationManager:self.locationManager];
            [self addChildViewController:self.localFinderViewController];
            [self.view addSubview:self.localFinderViewController.view];
        } else {
            self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
            self.locationManager.distanceFilter = 500; // meters
            [self.locationManager startUpdatingLocation];
        }
        
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyDoNotDisturb]) {
            [self.view addSubview:self.dndViewController.view];
            [self addChildViewController:self.dndViewController];
        }
        
        self.noUsersFoundView = [[NSBundle mainBundle] loadNibNamed:@"RDRNoUsersFound" owner:nil options:nil][0];
        [self.collectionView insertSubview:self.noUsersFoundView atIndex:0];
        self.noUsersFoundView.hidden = YES;
        
    } else {
        self.noFriendsFoundView = [[NSBundle mainBundle] loadNibNamed:@"RDRNoFriendsFound" owner:nil options:nil][0];
        [self.collectionView insertSubview:self.noFriendsFoundView atIndex:0];
        self.noFriendsFoundView.hidden = YES;
    }
    
    [self reloadFriendsListOnComplete:^{
        [self reloadUnreadMessages];
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)showLoadingView:(BOOL)show
{
    if (show) {
        if (self.loadingView.hidden) {
            self.loadingView.hidden = NO;
            
            const CGFloat kAnimationTime = 1.5;
            
            CABasicAnimation* rotationAnimation;
            rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
            rotationAnimation.fromValue = [self.scannerImageView.layer.presentationLayer valueForKeyPath:@"transform.rotation.z"];
            rotationAnimation.toValue = [NSNumber numberWithFloat: [rotationAnimation.fromValue floatValue] + M_PI * 2.0];
            rotationAnimation.duration = kAnimationTime;
            rotationAnimation.fillMode = kCAFillModeForwards;
            rotationAnimation.cumulative = YES;
            rotationAnimation.repeatCount = HUGE_VALF;
            
            [self.scannerImageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
            
        }
    } else {
        if (!self.loadingView.hidden) {
            self.loadingView.hidden = YES;
        }
    }
}

- (void)didBecomeCurrentViewController
{
    if (self.type == RDRMainHeaderViewTypeLocal) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyDoNotDisturb]) {
            [self addChildViewController:self.dndViewController];
            [self.view addSubview:self.dndViewController.view];
        } else {
            [self.dndViewController.view removeFromSuperview];
            [self.dndViewController removeFromParentViewController];
        }
    }
}

#pragma mark - IBActions

- (IBAction)onSettingsButtonTapped:(id)sender
{
    [self.collectionView setContentOffset:CGPointZero animated:NO];
    
    [self rdr_presentSettingsViewControllerOnDismiss:^(BOOL requireReload) {
        
        // First check user disconnected
        if ([[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyIsDisconnected]) {
            RDRConnectDeviceViewController *connectDeviceViewController = [RDRConnectDeviceViewController new];
            [self addChildViewController:connectDeviceViewController];
            [self.view addSubview:connectDeviceViewController.view];
        } else {
            if (self.type == RDRMainHeaderViewTypeLocal) {
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyDoNotDisturb]) {
                    [self.view addSubview:self.dndViewController.view];
                    [self addChildViewController:self.dndViewController];
                } else {
                    [self.dndViewController.view removeFromSuperview];
                    [self.dndViewController removeFromParentViewController];
                }
            }
            
            if (requireReload) {
                [self reloadFriendsListOnComplete:^{
                    [self reloadUnreadMessages];
                }];
            }
        }
    }];
}

- (void)onGenderFilterValueChanged:(UISegmentedControl *)segmentControl
{
    self.selectedGender = segmentControl.selectedSegmentIndex;
    self.filteredUsers = [[NSMutableArray alloc] initWithArray:self.localFriends];
    [self.filteredUsers removeObjectsInArray:self.radarFriends];
    if (self.selectedGender != RDRGenderAll) {
        [self.filteredUsers filterUsingPredicate:[NSPredicate predicateWithFormat:@"gender == %d", self.selectedGender]];
    }
    [self.collectionView reloadData];
}

- (IBAction)onRefreshButtonTapped:(id)sender
{
    // For local view only
    [self showLoadingView:YES];
    [self reloadFriendsListOnComplete:^{
        [self showLoadingView:NO];
        [self reloadUnreadMessages];
    }];
}

#pragma mark - Notification Method

- (void)onMessageReceivedNotification:(NSNotification *)notif
{
    NSString *senderPhoneNumber = notif.userInfo[@"sender"];
    NSString *messageId = notif.userInfo[@"messageId"];
    NSLog(@"sender phoneNumber: %@", senderPhoneNumber);
    
    NSUInteger __block index = NSNotFound;
    
    // TODO: if filter is turned on, this wont work
    [self.filteredUsers enumerateObjectsUsingBlock:^(RDRUser *user, NSUInteger idx, BOOL *stop) {
        if ([user.phoneNumber isEqualToString:senderPhoneNumber]) {
            [self appendIncomingMessage:messageId fromUserPhoneNumber:user.phoneNumber messageText:nil imageUrl:nil];
            *stop = YES;
            index = idx;
        }
    }];
    
    // Put this sender to the top of the collection view
    if (index != NSNotFound) {
        RDRUser *user = self.filteredUsers[index];
        [self.filteredUsers removeObjectAtIndex:index];
        [self.filteredUsers insertObject:user atIndex:0];
        [self.collectionView performBatchUpdates:^{
            [self.collectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:index inSection:0]]];
            [self.collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]]];
        } completion:^(BOOL finished) {
            if (self.type == RDRMainViewControllerTypeFriends) {
                [[RDRAddressBookController sharedAddressBookController] reorderFriendsListByPuttingFriendsOnTop:@[user] notify:NO];
                NSArray *array = [[RDRAddressBookController sharedAddressBookController] friendsList];
                self.radarFriends = [[NSMutableArray alloc] initWithArray:array];
            }
        }];
    }

}

- (void)onYourFriendJoinedNotification:(NSNotification *)notif
{
    [[RDRAddressBookController sharedAddressBookController] syncAddressBookOnComplete:^{
        [self reloadFriendsListOnComplete:nil];
    }];
}

- (void)onApplicationDidBecomeActive:(NSNotification *)notif
{
    NSLog(@"Reloading Messages from background");
    if (self.loadingView.hidden) {
        //if loading view is not hidden, assume we're already retreiving unread messages
        [self reloadFriendsListOnComplete:^{
            [self reloadUnreadMessages];
        }];
    }
}

- (void)onFriendsListUpdated:(NSNotification *)notif
{
    [self reloadFriendsListOnComplete:^{
        [self reloadUnreadMessages];
    }];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.filteredUsers.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * const CellIdentifier = @"thumbCell";
    RDRMainThumbnailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    RDRUser *user = self.filteredUsers[indexPath.item];
    
    if (RDRIsUsingDummyData) {
        cell.imageView.image = user.image;
    } else {
        [cell.imageView cancelImageRequestOperation];
        [cell.imageView setImageWithURL:[NSURL URLWithString:user.imageURLString] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        [cell.imageView setNeedsLayout];
        [cell.imageView setNeedsDisplay];
    }
    RDRMainThumbnailCellBorder border = RDRMainThumbnailCellBorderBottom;
    if (indexPath.item % 2 == 0) {
        border |= RDRMainThumbnailCellBorderRight;
    } else {
        border |= RDRMainThumbnailCellBorderLeft;
    }
    cell.border = border;
    
    cell.nameLabel.text = user.fullName;
    
    NSUInteger count = [[self.incomingMessages objectForKey:user.phoneNumber] count];
    cell.badgeView.badgeText = count > 0 ? @(count).stringValue : nil;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //get the object that corresponds to the selected user
    RDRUser *selectedUser = self.filteredUsers[indexPath.item];
    if(self.type == RDRMainHeaderViewTypeLocal) {
        selectedUser.userType = RDRLocalUserType;
    } else { selectedUser.userType = RDRContactUserType; }
    
    CGRect rect = [collectionView.collectionViewLayout layoutAttributesForItemAtIndexPath:indexPath].frame;
    rect = [self.view convertRect:rect fromView:collectionView];
    
    RDRMainThumbnailCell *cell = (RDRMainThumbnailCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyUseDummyData]) {
        
        NSString *phoneNumber = selectedUser.phoneNumber;
        NSMutableArray *messages = self.incomingMessages[phoneNumber];
        if (messages.count > 0) {
            [messages removeObjectAtIndex:0];
            [self saveMessages];
        }
        RDRSendChatViewController *sendChatVC = [[RDRSendChatViewController alloc] initWithRemoteUser:selectedUser enableCamera:self.type != RDRMainHeaderViewTypeLocal];
        sendChatVC.delegate = self;
        [self rdr_popUpViewController:sendChatVC fromRect:rect enablePanDismiss:NO onComplete:nil onDismiss:nil];
    } else {
        NSString *phoneNumber = selectedUser.phoneNumber;
        NSMutableArray *messages = self.incomingMessages[phoneNumber];
        if (messages.count > 0) {
            
            NSString *messageId = messages[0];
            
            void (^readMessageBlock)(NSString *message) = ^(NSString *message) {
                [self removeMessageWithId:messageId fromUser:selectedUser];
                RDRChatViewController *chatVC = [[RDRChatViewController alloc] initWithSender:selectedUser message:message];
                [self rdr_popUpViewController:chatVC fromRect:cell.frame enablePanDismiss:YES onComplete:Nil onDismiss:^{
                    [collectionView reloadData];
                }];
            };
            
            void (^viewImageBlock)(NSString *imageUrl) = ^(NSString *imageUrl) {
                RDRChatImageViewController *chatImageVC = [[RDRChatImageViewController alloc] initWithSender:selectedUser imageURL:imageUrl];
                [self rdr_popUpViewController:chatImageVC fromRect:cell.frame enablePanDismiss:YES onComplete:Nil onDismiss:^{
                    [collectionView reloadData];
                }];
                
                [self removeMessageWithId:messageId fromUser:selectedUser];
            };
            
            
            if (self.prefetchedMessages[messageId]) {
                NSLog(@"Prefetch Hit for messageID:%@", messageId);
                readMessageBlock(self.prefetchedMessages[messageId]);
            } else if (self.prefetchedImageMessages[messageId]) {
                NSLog(@"Prefetch Hit for image messageID:%@", messageId);
                viewImageBlock(self.prefetchedImageMessages[messageId]);
            } else {
                NSLog(@"Prefetch Miss for messageID:%@", messageId);
                [PFCloud callFunctionInBackground:@"getMessage" withParameters:@{@"messageId":messageId} block:^(id object, NSError *error) {
                    if(!error){
                        if (object[@"message"]) {
                            readMessageBlock(object[@"message"]);
                        } else if (object[@"pictureMessage"]) {
                            viewImageBlock(object[@"pictureMessage"]);
                        }
                        NSLog(@"%@ \n", object);
                    } else {
                        [self removeMessageWithId:messageId fromUser:selectedUser];
                        NSLog(@"get message failed..");
                        [collectionView reloadData];
                    }
                }];
            }
        } else {
            RDRSendChatViewController *sendChatVC = [[RDRSendChatViewController alloc] initWithRemoteUser:selectedUser enableCamera:self.type != RDRMainHeaderViewTypeLocal];
            sendChatVC.delegate = self;
            [self rdr_popUpViewController:sendChatVC fromRect:cell.frame enablePanDismiss:NO onComplete:nil onDismiss:nil];
        }
        
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    UIView *header = self.type == RDRMainHeaderViewTypeFriends ? self.sectionHeaderView : self.localHeaderView;
    CGRect frame = header.frame;
    CGFloat offsetY = scrollView.contentOffset.y;
    frame.origin.y = offsetY < 0 ? offsetY : 0.f;
    header.frame = frame;
    if (offsetY > _lastContentOffsetY) {
        _lastScrollDirection = 1;
    } else {
        _lastScrollDirection = -1;
    }
    _lastContentOffsetY = offsetY;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY > 0 && offsetY < 44.f) {
        if (_lastScrollDirection < 0) {
            [scrollView setContentOffset:CGPointMake(0.f, 0.f) animated:YES];
        } else {
            [scrollView setContentOffset:CGPointMake(0.f, 44.f) animated:YES];
        }
    }
}

#pragma mark - RDRSendChatViewControllerDelegate
- (void)sendChatViewControllerDidTapOnCloseButton:(RDRSendChatViewController *)vc
{
    [self rdr_dismissPopUpViewController];
}

- (void)sendChatViewControllerDidSendChat:(RDRSendChatViewController *)vc
{
    [self rdr_dismissPopUpViewController];
}

#pragma mark - Private
- (UIImage*)imageFromDiskWithFileName:(NSString*)fileName
{
    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", self.cacheDirectoryPath, fileName]];
    
    return image;
}

- (void)loadAndSaveImageToDiskFromURLString:(NSString*)urlString onComplete:(void (^)(NSString *fileName))onComplete
{
    // Fetch image, apply rounded corners and save to disk in background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        NSURLResponse *response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            
            // Save to Disk
            NSString *fileName = urlString.lastPathComponent;
            
            if ([[fileName.pathExtension lowercaseString] isEqualToString:@"png"]) {
                
                [UIImagePNGRepresentation(image) writeToFile:[self.cacheDirectoryPath stringByAppendingPathComponent:fileName] options:NSAtomicWrite error:nil];
                
            } else if ([[fileName.pathExtension lowercaseString] isEqualToString:@"jpg"] || [[fileName.pathExtension lowercaseString] isEqualToString:@"jpeg"]) {
                [UIImageJPEGRepresentation(image, 1.0) writeToFile:[self.cacheDirectoryPath stringByAppendingPathComponent:fileName] options:NSAtomicWrite error:nil];
            } else {
                NSLog(@"Unrecognized Path Extension");
            }
            
            // Callback on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                if (onComplete) {
                    onComplete(fileName);
                }
            });
        }
    });
}

- (void)reloadFriendsListOnComplete:(void (^)())onComplete //Or strangers for local view
{
    if (self.isReloading) {
        NSLog(@"Reload Guard Hit");
        return;
    }
    
    self.isReloading = YES;
    [self.messageSender stopAllMessages];
    self.filteredUsers = nil;
    [self.collectionView reloadData];
    
    if (self.type == RDRMainViewControllerTypeFriends) {
        
        NSArray *array = [[RDRAddressBookController sharedAddressBookController] friendsList];
        
        self.radarFriends = [[NSMutableArray alloc] initWithArray:array];
        
        self.filteredUsers = [[NSMutableArray alloc] initWithArray:self.radarFriends];
        [self.collectionView reloadData];
        self.isReloading = NO;
        self.noFriendsFoundView.hidden = self.filteredUsers.count > 0;
        if (onComplete) {
            onComplete();
        }
    } else if (self.isReadyForLocationUpdates) {
        NSArray *friends = [[RDRAddressBookController sharedAddressBookController] friendsList];
        self.radarFriends = [[NSMutableArray alloc] initWithArray:friends];
        
        [PFCloud callFunctionInBackground:@"getNearbyUsers" withParameters:@{@"latitude":@(self.currentLatitude), @"longitude":@(self.currentLongitude), @"phoneNumber":[RDRUser currentUser].phoneNumber ? : @"1234"} block:^(id object, NSError *error) {
            if(!error) {
                NSAssert([object isKindOfClass:[NSArray class]], @"Why did the programmer quit his job?........ Cuz he didn't get arrays!");
                [self.localFriends removeAllObjects];
                for (NSDictionary *d in object){
                    RDRUser *user = [[RDRUser alloc] initWithFirstName:d[@"firstName"] lastName:d[@"lastName"] phoneNumber:d[@"phoneNumber"] imageUrlString:d[@"picture"]];
                    user.gender = [d[@"gender"] intValue] == 1 ? RDRUserGenderMale : RDRUserGenderFemale;
                    [self.localFriends addObject:user];
                }
            } else {
                NSLog(@"get nearbyUsers failed..");
            }
            self.filteredUsers = [[NSMutableArray alloc] initWithArray:self.localFriends];
            if (self.selectedGender == RDRGenderMale) {
                [self.filteredUsers filterUsingPredicate:[NSPredicate predicateWithFormat:@"gender == %d", RDRUserGenderMale]];
            } else if (self.selectedGender == RDRGenderFemale) {
                [self.filteredUsers filterUsingPredicate:[NSPredicate predicateWithFormat:@"gender == %d", RDRUserGenderFemale]];
            }
            
            [self.filteredUsers removeObjectsInArray:self.radarFriends];
            
            [self.collectionView reloadData];
            self.isReloading = NO;
            
            self.noUsersFoundView.hidden = self.filteredUsers.count > 0;
            if (onComplete) {
                onComplete();
            }
        }];
    } else {
        self.needLocalFirstTimeRefresh = YES;
        self.isReloading = NO;
    }
}

- (void)onShareButtonTapped:(id)sender
{
    NSString *shareString = @"Hurry, come join me on Radar! It's the simplest way for us to stay connected :)";
    NSURL *shareUrl = [NSURL URLWithString:@"http://tryradar.com"];
    
    NSArray *activityItems = [NSArray arrayWithObjects:shareString, shareUrl, nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [self presentViewController:activityViewController animated:YES completion:nil];

}

- (void)startFakeIncomingMessages
{
    NSLog(@"Starting fake incoming messages!!");
    if (self.messageSender) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:RDRNotificationIncomingMessage object:self.messageSender];
        self.messageSender = nil;
    }
    self.messageSender = [RDRFakeMessasgeSender new];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageReceivedNotification:) name:RDRNotificationIncomingMessage object:self.messageSender];
    NSAssert(self.dummyUsers.count >= 10, @"Didn't initialize dummy users");
    [self.messageSender startFakeMessagesFromUser:self.dummyUsers.allValues[6] timeInterval:8.f];
    [self.messageSender startFakeMessagesFromUser:self.dummyUsers.allValues[8] timeInterval:13.f];
    [self.messageSender startFakeMessagesFromUser:self.dummyUsers.allValues[10] timeInterval:11.f];
}

- (void)saveMessages
{
    if(self.type == RDRMainHeaderViewTypeFriends){
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        path = [path stringByAppendingPathComponent:@"messagesFromFriends.plist"];
    
        if(![self.incomingMessages writeToFile:path atomically:YES]) {
            NSLog(@"Write messages to Friends file failed..");
        }
    }else{
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        path = [path stringByAppendingPathComponent:@"messagesFromLocals.plist"];
        
        if(![self.incomingMessages writeToFile:path atomically:YES]) {
            NSLog(@"Write messages to Locals file failed..");
        }
    }
}

- (BOOL)loadMessages
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    if(self.type == RDRMainHeaderViewTypeFriends){
        path = [path stringByAppendingPathComponent:@"messagesFromFriends.plist"];
    }else{
        path = [path stringByAppendingPathComponent:@"messagesFromLocals.plist"];
    }
    
    NSMutableDictionary *friendsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    if (friendsDictionary) {
        self.incomingMessages = friendsDictionary;
        return YES;
    } else {
        return NO;
    }
}

- (void)reloadUnreadMessages
{
    if (self.isReloading) {
        NSLog(@"Reload Guard hit for unread msgs");
        return;
    }
    
    NSArray *friendsList = (self.type == RDRMainViewControllerTypeFriends) ? self.radarFriends : self.localFriends;
    if (friendsList.count == 0) {
        return;
    }
    
    self.isReloading = YES;
    [self showLoadingView:YES];
    NSString *commaSeparatedNumbers = [[friendsList valueForKeyPath:@"phoneNumber"] componentsJoinedByString:@","];
    NSMutableSet *usersWithMessages = [NSMutableSet new];
    [PFCloud callFunctionInBackground:@"getUnreadMessages"
                       withParameters:@{@"senderString": commaSeparatedNumbers, @"receiver":[RDRUser currentUser].phoneNumber}
                                block:^(NSArray *responseMessages, NSError *error) {
                                    
                                    if(!error){
                                        NSCAssert([responseMessages isKindOfClass:[NSArray class]], @"didn't get arrays yo");
                                        
                                        [UIApplication sharedApplication].applicationIconBadgeNumber = [responseMessages count];
                                        
                                        NSArray *phoneNumbers = [friendsList valueForKeyPath:@"phoneNumber"];
                                        for (NSDictionary *msgDict in responseMessages) {
                                            NSString *senderId = msgDict[@"senderId"];
                                            if ([phoneNumbers containsObject:senderId]) {
                                                [self appendIncomingMessage:msgDict[@"objectId"] fromUserPhoneNumber:senderId messageText:msgDict[@"message"] imageUrl:msgDict[@"imageUrl"]];
                                                
                                                NSInteger index = [phoneNumbers indexOfObject:senderId];
                                                [usersWithMessages addObject:friendsList[index]];
                                            } else {
                                                NSLog(@"received msg not from contact list");
                                            }
                                        }
                                        
                                        NSLog (@"Retreived %lu unread message(s) from %lu users (%@)", (unsigned long) responseMessages.count, (unsigned long)usersWithMessages.count, self.type == RDRMainViewControllerTypeFriends ? @"Friends":@"Local");
                                    } else {
                                        NSLog(@"get unread messages failed");
                                    }
                                    
                                    // Iterated to the end
                                    [self showLoadingView:NO];
                                    [self saveMessages];
                                    
                                    [usersWithMessages intersectSet:[NSSet setWithArray:self.filteredUsers]];
                                    
                                    
                                    [self.filteredUsers removeObjectsInArray:usersWithMessages.allObjects];
                                    NSIndexSet *insertionIndices = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, usersWithMessages.count)];
                                    [self.filteredUsers insertObjects:usersWithMessages.allObjects atIndexes:insertionIndices];
                                    
                                    if (self.type == RDRMainViewControllerTypeFriends) {
                                        // Save the new order to disk
                                        [[RDRAddressBookController sharedAddressBookController] reorderFriendsListByPuttingFriendsOnTop:usersWithMessages.allObjects notify:NO];
                                    }
                                    
                                    
                                    [self.collectionView reloadData];
                                    
                                    self.isReloading = NO;
                                }];
}

- (void)appendIncomingMessage:(NSString *)messageId fromUserPhoneNumber:(NSString *)userNum messageText:(NSString *)txt imageUrl:(NSString *)imageUrl
{
    NSMutableArray *messages = self.incomingMessages[userNum];
    if(!messages){
        messages = [NSMutableArray new];
        self.incomingMessages[userNum] = messages;
    }
    
    if (![messages containsObject:messageId]) {
        
        [messages addObject:messageId];
        [self saveMessages];
    }
    
    if (txt) {
        self.prefetchedMessages[messageId] = txt;
    } else if(imageUrl) {
        self.prefetchedImageMessages[messageId] = imageUrl;
    } else {
        [self prefetchMessageWithId:messageId];
    }
}

- (void)removeMessageWithId:(NSString *)messageId fromUser:(RDRUser *)user
{
    NSAssert(user, @"user is nil yo");
    NSAssert(messageId, @"message id cant be nil");
    
    // Send a message was read notification
    [PFCloud callFunctionInBackground:@"readMessageD"
                       withParameters:@{@"messageObjectId":messageId}
                                block:^(id object, NSError *error) {
        if(error) {
            NSLog(@"Set Message Read Failed: %@", error);
        }else{
            [UIApplication sharedApplication].applicationIconBadgeNumber--;
        }
    }];
    
    NSMutableArray *messages = self.incomingMessages[user.phoneNumber];
    if (messages) {
        [messages removeObject:messageId];
        [self saveMessages];
        NSLog(@"write file remove message success...");
    }
    
    [self.prefetchedMessages removeObjectForKey:messageId];
    [self.prefetchedImageMessages removeObjectForKey:messageId];
}

- (void)prefetchMessageWithId:(NSString *)messageId
{
    if (!self.prefetchedMessages[messageId] && !self.prefetchedImageMessages[messageId] && !RDRIsUsingDummyData) {
        [PFCloud callFunctionInBackground:@"getMessage" withParameters:@{@"messageId":messageId} block:^(id object, NSError *error) {
            if(!error){
                if (object[@"message"]) {
                    self.prefetchedMessages[messageId] = object[@"message"];
                } else if (object[@"pictureMessage"]) {
                    self.prefetchedImageMessages[messageId] = object[@"pictureMessage"];
                }
            } else {
                NSLog(@"prefetch message %@ failed..", messageId);
            }
        }];
    }
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Location Update Failed %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSLog(@"%@, status: %d", NSStringFromSelector(_cmd), status);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (self.localFinderViewController) {
        [UIView animateWithDuration:0.4f animations:^{
            CGRect frame = self.localFinderViewController.view.frame;
            frame.origin.y = frame.size.height;
            self.localFinderViewController.view.frame = frame;
        } completion:^(BOOL finished) {
            [self.localFinderViewController.view removeFromSuperview];
            [self.localFinderViewController removeFromParentViewController];
            self.localFinderViewController = nil;
        }];
    }
    
    
    // If it's a relatively recent event, turn off updates to save power.
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 20.0) {
        // If the event is recent, do something with it.
        NSLog(@"latitude %+.6f, longitude %+.6f\n",
              location.coordinate.latitude,
              location.coordinate.longitude);
        
        self.currentLatitude = location.coordinate.latitude;
        self.currentLongitude = location.coordinate.longitude;
        
        //below is Test code for cloud code Update Location and get nearby Users
        [PFCloud callFunctionInBackground:@"updateLocation" withParameters:@{@"latitude":@(self.currentLatitude), @"longitude":@(self.currentLongitude), @"phoneNumber":[RDRUser currentUser].phoneNumber ? : @"1234"} block:^(id object, NSError *error) {
            if(!error){
                NSLog (@"Sent updated location");
            }else{
                NSLog(@"updateLocation failed: %@", error);
            }
        }];
        
        self.isReadyForLocationUpdates = YES;
        [self reloadFriendsListOnComplete:^{
            if (self.needLocalFirstTimeRefresh) {
                self.needLocalFirstTimeRefresh = NO;
                [self reloadUnreadMessages];
            }
        }];
    }
}

@end
