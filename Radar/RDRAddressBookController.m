//
//  RDRAddressBookController.m
//  Radar
//
//  Created by Rollin Su on 3/23/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRAddressBookController.h"
#import "RDRUser.h"
#import <AddressBook/AddressBook.h>
#import <Parse/Parse.h>
#import <UIAlertView-Blocks/UIAlertView+Blocks.h>


@interface RDRAddressBookController()
@property (nonatomic, strong) NSArray *friendsList;
@property (nonatomic, strong) NSString *commaSeparatedNumbers;
@end

@implementation RDRAddressBookController

+ (instancetype)sharedAddressBookController
{
    static RDRAddressBookController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self class] new];
    });
    return sharedInstance;
}

- (NSArray *)getAddressBookPhoneNumbers
{
    // Request authorization to Address Book
    NSMutableArray *numbers = [NSMutableArray new];
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
                for(CFIndex i = 0; i < CFArrayGetCount(people); i++){
                    ABRecordRef record = CFArrayGetValueAtIndex(people, i);
                    ABMultiValueRef phoneNumbers = ABRecordCopyValue(record, kABPersonPhoneProperty);
                    NSString *label;
                    for(CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++){
                        label = (__bridge NSString *)(ABMultiValueCopyLabelAtIndex(phoneNumbers, i));
                        if([label isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        } else if ([label isEqualToString:(NSString *)kABPersonPhoneMobileLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        } else if([label isEqualToString:(NSString *)kABPersonPhoneMainLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        }else if([label isEqualToString:(NSString *)kABHomeLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        }else if([label isEqualToString:(NSString *)kABWorkLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        }
                    }
                }
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Please go to Settings > Privacy > Contacts to give Radar access to your contacts." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                [alert show];
            }
        });
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
        for(CFIndex i = 0; i < CFArrayGetCount(people); i++){
            ABRecordRef record = CFArrayGetValueAtIndex(people, i);
            ABMultiValueRef phoneNumbers = ABRecordCopyValue(record, kABPersonPhoneProperty);
            NSString *label;
            for(CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++){
                label = (__bridge NSString *)(ABMultiValueCopyLabelAtIndex(phoneNumbers, i));
                if([label isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }else if([label isEqualToString:(NSString *)kABPersonPhoneMobileLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }else if([label isEqualToString:(NSString *)kABPersonPhoneMainLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }else if([label isEqualToString:(NSString *)kABHomeLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }else if([label isEqualToString:(NSString *)kABWorkLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }
            }
        }
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Please go to Settings > Privacy > Contacts to give Radar access to your contacts." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"We can't access your contacts. You may have parental control enabled" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
    return numbers;
}

- (NSString *)normalizedPhoneNumbers:(NSArray *)phoneNumbers
{
    NSMutableArray *normalizedNumbers = [NSMutableArray new];
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"+- ()\u00a0"];
    
    for (NSString *n in phoneNumbers) {
        NSString *foo = [[n componentsSeparatedByCharactersInSet:charSet] componentsJoinedByString:@""];
        if(foo.length == 11){ //dropping US country code from 11 digit numbers
            foo = [foo substringFromIndex:1]; //TODO: handle intl' numbers
        }
        [normalizedNumbers addObject:foo];
    }
    
    return [normalizedNumbers componentsJoinedByString:@","];
}

- (void)getExistingUsersOnRadarWithPhoneNumbers:(NSString *) phoneNumbers onComplete:(void (^)(id object, NSError *error))onComplete {
    
    if (phoneNumbers.length == 0) {
        NSLog(@"Contact list is empty");
        return;
    }
    NSString *myPhoneNumber = [RDRUser currentUser].phoneNumber;
    NSString *myName = [RDRUser currentUser].fullName;
    
    [PFCloud callFunctionInBackground:@"syncAddressBook" withParameters:@{@"addressBook":phoneNumbers, @"phoneNumber":myPhoneNumber, @"myName":myName} block:^(id object, NSError *error) {
        if (onComplete) {
            onComplete(object, error);
        }
    }];
}

- (NSArray *)loadFriendsListFromDisk
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    path = [path stringByAppendingPathComponent:@"friends.plist"];
    
    NSArray *friends = [NSArray arrayWithContentsOfFile:path];
    if (!friends) {
        NSLog(@"No friends found");
        return nil;
    } else {
        return friends;
    }
}

- (void)saveFriendsList
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    path = [path stringByAppendingPathComponent:@"friends.plist"];
    
    NSMutableArray *savable = [NSMutableArray new];
    for (RDRUser *user in _friendsList) {
        [savable addObject:[user dictionaryRepresentation]];
    }
    if(![savable writeToFile:path atomically:YES]) {
        NSLog(@"Write friends file failed..");
    }
}

- (void)updateFriendsListFromResponse:(NSArray *)friendsResponse
{
    if (!friendsResponse) {
        friendsResponse = [NSArray new]; //if nil is passed, save an empty array
    }
    
    self.friendsList = nil;
    NSMutableArray *friends = [NSMutableArray new];
    for (NSDictionary *d in friendsResponse) {
        RDRUser *user = [[RDRUser alloc] initWithFirstName:d[@"firstName"] lastName:d[@"lastName"] phoneNumber:d[@"phoneNumber"] imageUrlString:d[@"picture"]];
        user.gender = [d[@"gender"] intValue] == 1 ? RDRUserGenderMale : RDRUserGenderFemale;
        [friends addObject:user];
    }
    self.friendsList = friends;
    
    [self saveFriendsList];
    [[NSNotificationCenter defaultCenter] postNotificationName:RDRNotificationFriendsListUpdated object:self];
}

#pragma mark - Public Methods
- (void)syncAddressBookOnComplete:(void (^)(void))onComplete
{
    NSArray *rawNumbers = [self getAddressBookPhoneNumbers];
    if (rawNumbers.count == 0) {
        [self updateFriendsListFromResponse:[NSArray new]]; //save an empty array
        if (onComplete) {
            onComplete();
        }
    } else {
        [self getExistingUsersOnRadarWithPhoneNumbers:[self normalizedPhoneNumbers:rawNumbers] onComplete:^(id object, NSError *error) {
            if(!error) {
                [self updateFriendsListFromResponse:object];
            } else {
                NSLog(@"syncAddressBook failed");
            }
            if (onComplete) {
                onComplete();
            }
        }];
    }
}

- (NSArray *)friendsList
{
    if (!_friendsList) {
        NSArray *friendsFromDisk = [self loadFriendsListFromDisk];
        NSMutableArray *friends = [NSMutableArray new];
        for (NSDictionary *d in friendsFromDisk) {
            RDRUser *user = [[RDRUser alloc] initWithFirstName:d[@"firstName"] lastName:d[@"lastName"] phoneNumber:d[@"phoneNumber"] imageUrlString:d[@"picture"]];
            [friends addObject:user];
        }
        _friendsList = [[NSArray alloc] initWithArray:friends];
        [[NSNotificationCenter defaultCenter] postNotificationName:RDRNotificationFriendsListUpdated object:self];
    }
    return _friendsList;
}


- (void) reorderFriendsListByPuttingFriendsOnTop:(NSArray *)friends notify:(BOOL)notify
{
    if (friends.count == 0) {
        return;
    }
    NSMutableArray *mutableFriends = [_friendsList mutableCopy];
    [mutableFriends removeObjectsInArray:friends];
    NSIndexSet *insertionIndices = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, friends.count)];
    [mutableFriends insertObjects:friends atIndexes:insertionIndices];
    _friendsList = [NSArray arrayWithArray:mutableFriends];
    [self saveFriendsList];
    if (notify) {
        [[NSNotificationCenter defaultCenter] postNotificationName:RDRNotificationFriendsListUpdated object:self];
    }
}

- (NSString *)friendsListNumbers
{
    NSArray *friendsList = [self friendsList];
    NSMutableArray *friendsPhoneNumbers = [NSMutableArray new];
    
    if(!_commaSeparatedNumbers){
        _commaSeparatedNumbers = @"";
        for(RDRUser *user in friendsList){
            [friendsPhoneNumbers addObject:user.phoneNumber];
        }
        _commaSeparatedNumbers = [friendsPhoneNumbers componentsJoinedByString:@","];
        //NSLog(@"comma spearated numbers: %@", _commaSeparatedNumbers);
    }
    return _commaSeparatedNumbers;
}

@end
