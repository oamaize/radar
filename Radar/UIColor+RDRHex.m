//
//  UIColor+UIColor_RDRHex.m
//  Radar
//
//  Created by Rollin Su on 12/7/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "UIColor+RDRHex.h"

@implementation UIColor (RDRHex)

+ (UIColor*)rdr_colorWithHexLiteral:(NSUInteger)hexLiteral
{
    CGFloat red = ((hexLiteral >> 16) & 0xFF)/255.f;
    CGFloat green = ((hexLiteral >> 8) & 0xFF)/255.f;
    CGFloat blue = (hexLiteral & 0xFF)/255.f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.f];
}

@end
