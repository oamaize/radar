//
//  UIImage+RDRBlur.m
//  Radar
//
//  Created by Rollin Su on 1/22/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "UIImage+RDRBlur.h"
#import <Accelerate/Accelerate.h>
#import <GPUImage/GPUImage.h>

@implementation UIImage (RDRBlur)

- (UIImage*)rdr_blurredImageWithRadius:(CGFloat)radius
{
    //return [self rdr_vImageBlurredImageWithRadius:radius];
    return [self rdr_gpuBlurredImageWithRadius:radius];
}

- (UIImage*)rdr_gpuBlurredImageWithRadius:(CGFloat)radius
{
    GPUImageiOSBlurFilter *blurFilter = [[GPUImageiOSBlurFilter alloc] init];
    return [blurFilter imageByFilteringImage:self];
    //Read more at http://blog.bubbly.net/2013/09/11/slick-tricks-for-ios-blur-effect/#sqljGpIoDYrw57ac.99
}

- (UIImage*)rdr_vImageBlurredImageWithRadius:(CGFloat)radius
{
    if ((radius < 0.0f) || (radius > 1.0f)) {
        radius = 0.5f;
    }
    int boxSize = (int)(radius * 100);
    boxSize -= (boxSize % 2) + 1;
    CGImageRef rawImage = self.CGImage;
    vImage_Buffer inBuffer, outBuffer;
    vImage_Error error;
    void *pixelBuffer;
    CGDataProviderRef inProvider = CGImageGetDataProvider(rawImage);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    inBuffer.width = CGImageGetWidth(rawImage);
    inBuffer.height = CGImageGetHeight(rawImage);
    inBuffer.rowBytes = CGImageGetBytesPerRow(rawImage);
    inBuffer.data = (void*)CFDataGetBytePtr(inBitmapData);
    pixelBuffer = malloc(CGImageGetBytesPerRow(rawImage) * CGImageGetHeight(rawImage));
    outBuffer.data = pixelBuffer;
    outBuffer.width = CGImageGetWidth(rawImage);
    outBuffer.height = CGImageGetHeight(rawImage);
    outBuffer.rowBytes = CGImageGetBytesPerRow(rawImage);
    error = vImageBoxConvolve_ARGB8888(&inBuffer, &outBuffer, NULL, 0, 0, boxSize, boxSize, NULL, kvImageEdgeExtend);
    if (error) {
        NSLog(@"error from convolution %ld", error);
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(outBuffer.data, outBuffer.width, outBuffer.height, 8, outBuffer.rowBytes, colorSpace, CGImageGetBitmapInfo(self.CGImage));
    CGImageRef imageRef = CGBitmapContextCreateImage (ctx);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGContextRelease(ctx);
    CGColorSpaceRelease(colorSpace);
    free(pixelBuffer);
    CFRelease(inBitmapData);
    CGImageRelease(imageRef);
    return returnImage;
}

@end
