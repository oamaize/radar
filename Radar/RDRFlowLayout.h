//
//  RDRFlowLayout.h
//  Radar
//
//  Created by Rollin Su on 2/2/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDRFlowLayout : UICollectionViewFlowLayout

@end
