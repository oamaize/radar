//
//  main.m
//  Radar
//
//  Created by Ota-Benga Amaize on 11/1/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDRRadarAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RDRRadarAppDelegate class]));
    }
}
