//
//  RDRSignUpViewController.m
//  Radar
//
//  Created by Rollin Su on 11/19/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRSignUpViewController.h"
#import "RDRPhotoCaptureViewController.h"
#import "RDRSmsVerifyViewController.h"
#import "RDRGenderSelectionViewController.h"
#import "RDRMasterPageViewController.h"
#import "RDRConnectDeviceViewController.h"
#import "RDRUser.h"
#import "UIView+RDRTheme.h"

#import <MessageUI/MessageUI.h>

static NSString * const RDRDefaultsKeyIsInVerificationProcess = @"RDRDefaultsKeyIsInVerificationProcess";

@interface RDRSignUpViewController () <RDRGenderSelectionViewControllerDelegate, RDRPhotoCaptureViewControllerDelegate, RDRSmsVerifyViewControllerDelegate, UIScrollViewDelegate, MFMessageComposeViewControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (strong, nonatomic) RDRGenderSelectionViewController *genderSelectionViewController;
@property (strong, nonatomic) RDRPhotoCaptureViewController *photoCaptureViewController;
@property (strong, nonatomic) RDRSmsVerifyViewController *smsVerifyViewController;

@property (strong, nonatomic) RDRUser *signupUser;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UILabel *almostDoneLabel;


@end

@implementation RDRSignUpViewController

- (id)init
{
    self = [super initWithNibName:@"RDRSignUpViewController" bundle:nil];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.signupUser = [RDRUser new];
    
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width * 3, self.scrollView.bounds.size.height);
    
    self.photoCaptureViewController = [RDRPhotoCaptureViewController new];
    self.photoCaptureViewController.delegate = self;
    [self addChildViewController:self.photoCaptureViewController];
    
    self.genderSelectionViewController = [RDRGenderSelectionViewController new];
    self.genderSelectionViewController.delegate = self;
    [self addChildViewController:self.genderSelectionViewController];
    
    self.smsVerifyViewController = [RDRSmsVerifyViewController new];
    self.smsVerifyViewController.delegate = self;
    [self addChildViewController:self.smsVerifyViewController];
    
    [self addView:self.photoCaptureViewController.view toPagerAtIndex:0];
    [self addView:self.genderSelectionViewController.view toPagerAtIndex:1];
    [self addView:self.smsVerifyViewController.view toPagerAtIndex:2];
    
    self.title = @"radar";
    
    self.nextButton.enabled = NO;
    self.almostDoneLabel.hidden = YES;
    
    
    RDRConnectDeviceViewController *vc = [RDRConnectDeviceViewController new];
    [self addChildViewController:vc];
    
    vc.view.frame = self.view.bounds;
    [self.view addSubview:vc.view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onVerificationNotification:) name:RDRNotificationDeviceVerified object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - IBActions
- (IBAction)onNextButtonTapped:(id)sender {
    self.nextButton.enabled = NO;
    if (self.scrollView.contentOffset.x >= self.scrollView.bounds.size.width) {
        self.nextButton.hidden = YES;
        self.almostDoneLabel.hidden = NO;
    }
    
    CGPoint offset = self.scrollView.contentOffset;
    offset.x += self.scrollView.bounds.size.width;
    
    if(offset.x == self.scrollView.bounds.size.width*2) [self.smsVerifyViewController.firstNameField becomeFirstResponder];
    
    [self.scrollView setContentOffset:offset animated:YES];
}

- (IBAction)onSkipButtonTapped:(id)sender {
    [self onSkipSuccess];
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
}

#pragma mark - Gender Delegate Method
- (void)genderController:(RDRGenderSelectionViewController *)controller didSelectUserGender:(RDRUserGender)gender
{
    self.signupUser.gender = gender;
    self.nextButton.enabled = YES;
}


#pragma mark - Photo Delegate Method

- (void)photoCaptureViewControllerDidCapturePhoto:(RDRPhotoCaptureViewController*)photoCaptureViewController photo:(UIImage *)image{
    self.signupUser.image = image;
    self.nextButton.enabled = YES;
}

#pragma mark - SMS Delegate Method
- (void)smsVerifyViewController:(RDRSmsVerifyViewController *)vc didSubmitVerificationWithFirstName:(NSString *)firstName lastName:(NSString *)lastName
{
    [self.spinner startAnimating];
    self.signupUser.firstName = firstName;
    self.signupUser.lastName = lastName;
    [self createUser];
}

#pragma mark - MFMessageCompoerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"You must verify your phone number to continue." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [alert show];
            [self.smsVerifyViewController reset];
            [self.spinner stopAnimating];
            break;
        }
		case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"The SMS failed to send. Please try again." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [alert show];
            [self.smsVerifyViewController reset];
            [self.spinner stopAnimating];
        }
			break;
		case MessageComposeResultSent:
        {
            // Now we just wait for verification call back push
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:RDRDefaultsKeyIsInVerificationProcess];
            [[NSUserDefaults standardUserDefaults] synchronize];
            double delayInSeconds = 10.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                if ([[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyIsInVerificationProcess]) {
                    // Failzz
                    
                    [PFCloud callFunctionInBackground:@"testPhoneVerification" withParameters:@{@"pin":[[NSUserDefaults standardUserDefaults] objectForKey:kRadarPin]} block:^(id object, NSError *error) {
                        if(!error){
                            if([object[@"numberVerified"] isEqualToString:@"1"]){
                                [self onVerificationSuccess:object[@"phoneNumber"]];
                            }else{
                                [[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"We weren't able to verify your device. Please try again later." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
                                [self.spinner stopAnimating];
                                [self.smsVerifyViewController reset];
                            }
                        }
                    }];
                }
            });
        }
            break;
		default:
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1111) {
        abort();
    }
}

#pragma mark - Private
- (void)createUser
{
    // 1 for male, 2 for female
    NSNumber *genderNumber = self.signupUser.gender==RDRGenderMale? @1:@2;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.signupUser.image.size.width*0.4, self.signupUser.image.size.height*0.4), NO, 1.0);
    [self.signupUser.image drawInRect:CGRectMake(0,0, self.signupUser.image.size.width*0.4, self.signupUser.image.size.height*0.4)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImagePNGRepresentation(newImage);
    PFFile *imageFile = [PFFile fileWithName:@"ownpic.png" data:imageData];
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            [PFCloud callFunctionInBackground:@"createUser"
                               withParameters:@{@"gender":genderNumber, @"firstName":self.signupUser.firstName, @"lastName":self.signupUser.lastName}
                                        block:^(id object, NSError *error) {
                                            if(!error) {
                                                object[@"picture"] = imageFile;
                                                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                                    if(succeeded){
                                                        self.signupUser.imageURLString = ((PFFile *)object[@"picture"]).url;
                                                    }
                                                }];
                                                [self savePinToInstallation:object[@"pin"]];
                                                [self savePinToUserDefaults:object[@"pin"]];
                                                [self sendVerificationSMSWithPin:object[@"pin"]];
                                            } else {
                                                NSLog(@"createUser failed..");
                                                
                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"We're having trouble creating your account. Please try again." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                                [alert show];
                                                [self.smsVerifyViewController reset];
                                                [self.spinner stopAnimating];
                                            }
                                            
                                        }];
        }
    }];
}

- (void)savePinToInstallation:(NSString*)pin
{
    PFInstallation *installation = [PFInstallation currentInstallation];
    installation[@"pin"] = pin;
    [installation saveInBackground];
    NSLog(@"saved pin to user installation in server: %@", pin);
}

- (void)savePinToUserDefaults:(NSString *)pin
{
    [[NSUserDefaults standardUserDefaults] setObject:pin forKey:kRadarPin];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"saved pin to user defaults: %@", pin);
}

- (void)savePhoneToInstallation:(NSString*)phoneNumber
{
    PFInstallation *installation = [PFInstallation currentInstallation];
    installation[@"phoneNumber"] = phoneNumber;
    [installation saveInBackground];
    NSLog(@"saved phone number to user installation in server: %@", phoneNumber);
}

- (void)sendVerificationSMSWithPin:(NSString*)pin
{
    if([MFMessageComposeViewController canSendText])
    {
        MFMessageComposeViewController *msgComposer = [MFMessageComposeViewController new];
        NSString *text = [NSString stringWithFormat:@"Send this text to verify your number: %@", pin];
        msgComposer.body = text;
        
        /*staging twilio number*/
        msgComposer.recipients = [NSArray arrayWithObjects:@"6093575019", nil];
        
        /*prouction twilio number
        msgComposer.recipients = [NSArray arrayWithObjects:@"6096080735", nil];
        */

        msgComposer.messageComposeDelegate = self;
        [self presentViewController:msgComposer animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You must run this app on a device that can send SMS." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        alert.tag = 1111;
        [self.spinner stopAnimating];
        [alert show];
    }

}

- (void)onVerificationNotification:(NSNotification*)notif
{
    [self onVerificationSuccess:notif.userInfo[@"phoneNumber"]];
}

- (void)onVerificationSuccess:(NSString *)phoneNumber
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:RDRDefaultsKeyIsInVerificationProcess];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"phoneNumber from verification Push: %@", phoneNumber);
    
    [self savePhoneToInstallation:phoneNumber];
    
    RDRUser *user = [[RDRUser alloc] initWithFirstName:_signupUser.firstName lastName:_signupUser.lastName phoneNumber:phoneNumber imageUrlString:_signupUser.imageURLString];
    [RDRUser setCurrentUser:user];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[RDRUser currentUser].imageURLString]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [RDRUser currentUser].image = image;
        });
    });
    
    self.nextButton.enabled = YES;
    [self.spinner stopAnimating];
    
    UIView *splash = [[UINib nibWithNibName:@"SplashView" bundle:nil] instantiateWithOwner:nil options:nil][0];
    splash.alpha = 0.f;
    [self.view addSubview:splash];
    [UIView animateWithDuration:1.f animations:^{
        splash.alpha = 1.f;
    } completion:^(BOOL finished) {
        double delayInSeconds = 1.f;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self dismissViewControllerAnimated:YES completion:nil];
        });
    }];
}

- (void)onSkipSuccess
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:RDRDefaultsKeyIsInVerificationProcess];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"skip pressed. my info exists..");
    if (![RDRUser currentUser]) {
        RDRUser *user = [RDRUser userFromDisk];
        if (!user) {
            NSLog(@"No user found. Creating dummy user");
            
            user = [[RDRUser alloc] initWithFirstName:@"John"
                                             lastName:@"Doe"
                                          phoneNumber:@"12345678444"
                                       imageUrlString:@"http://fe867b.medialib.glogster.com/media/65/65ac4e2ecaf1c74a2eab2bc00d43d132c6590b4617fbb9257c5620027268a22d/homer6big.jpg"];
        }
        [RDRUser setCurrentUser:user];
    }
    
    if ([RDRUser currentUser].imageURLString.length > 0) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[RDRUser currentUser].imageURLString]]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [RDRUser currentUser].image = image;
            });
        });
    }
    
    self.nextButton.enabled = YES;
    [self.spinner stopAnimating];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addView:(UIView*)view toPagerAtIndex:(NSInteger)index
{
    CGRect frame = self.scrollView.bounds;
    frame.origin.x = index * self.scrollView.bounds.size.width;
    view.frame = frame;
    [self.scrollView addSubview:view];
}


@end
