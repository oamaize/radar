//
//  RDRSmsVerifyViewController.m
//  Radar
//
//  Created by Ota-Benga Amaize on 11/16/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRSmsVerifyViewController.h"
#import "UIView+RDRTheme.h"

#import <Parse/Parse.h>
#import <MessageUI/MessageUI.h>


@interface RDRSmsVerifyViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;

@end

NSString * const kRadarUUIDKey2 = @"kRadarUUIDKey";

@implementation RDRSmsVerifyViewController


- (id)init
{
    self = [super initWithNibName:@"RDRSmsVerifyViewController" bundle:nil];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.firstNameField.delegate = self;
    self.lastNameField.delegate = self;
    self.verifyButton.enabled = NO;
    [self.verifyButton rdr_applyInactiveButtonTheme];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private

- (void)reset
{
    self.firstNameField.enabled = YES;
    self.lastNameField.enabled = YES;
    
    if (self.firstNameField.text.length == 0 || self.lastNameField.text.length == 0) {
        self.verifyButton.enabled = NO;
        [self.verifyButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
        [self.verifyButton rdr_applyInactiveButtonTheme];
    } else {
        self.verifyButton.enabled = YES;
        [self.verifyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.verifyButton rdr_applyButtonTheme];
    }
}

#pragma mark - UITextFieldDelegateMethods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.firstNameField) {
        [self.lastNameField becomeFirstResponder];
    } else {
        [self.lastNameField resignFirstResponder];
        return YES;
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.firstNameField) {
        if((str.length == 0) || (self.lastNameField.text.length == 0)){
            [self.verifyButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
            [self.verifyButton rdr_applyInactiveButtonTheme];
            self.verifyButton.enabled = NO;
        }else{
            [self.verifyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.verifyButton rdr_applyButtonTheme];
            self.verifyButton.enabled = YES;
        }
    } else if (textField == self.lastNameField) {
        if((str.length == 0) || (self.firstNameField.text.length == 0)){
            [self.verifyButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
            [self.verifyButton rdr_applyInactiveButtonTheme];
            self.verifyButton.enabled = NO;
        }else{
            [self.verifyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.verifyButton rdr_applyButtonTheme];
            self.verifyButton.enabled = YES;
        }
    }
    return YES;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField{
    
    [self.verifyButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    [self.verifyButton rdr_applyInactiveButtonTheme];
    self.verifyButton.enabled = NO;
    
    return YES;
}

#pragma mark - IBActions
- (IBAction)onVerifyButtonTapped:(id)sender {
    [self.lastNameField resignFirstResponder];
    [self.verifyButton rdr_applyInactiveButtonTheme];
    self.verifyButton.enabled = NO;
    [self.verifyButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    
    self.firstNameField.enabled = NO;
    self.lastNameField.enabled = NO;

    [self.delegate smsVerifyViewController:self didSubmitVerificationWithFirstName:self.firstNameField.text lastName:self.lastNameField.text];}

- (IBAction)onWhyButtonTapped:(id)sender {
    
}

@end
