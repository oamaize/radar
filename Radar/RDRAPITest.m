//
//  RDRAPITest.m
//  Radar
//
//  Created by Rollin Su on 3/3/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRAPITest.h"
#import <Parse/Parse.h>

@interface RDRAPITest()

@property (nonatomic, strong) NSString *cacheDirectoryPath;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *radarFriends;
@property (weak, nonatomic) IBOutlet UIButton *importContactsButton;
@end

@implementation RDRAPITest


- (void)findExistingUsers{
    NSMutableArray *addressBook = [NSMutableArray new];
    addressBook[0] = @"16093570000";
    addressBook[1] = @"16093570001";
    addressBook[2] = @"16093570002";
    
    NSString *myUUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"kRadarUUIDKey"];
    
    NSString *testString = @"16093570000,16093570001,16093570002";
    
    [PFCloud callFunctionInBackground:@"findExistingUsers" withParameters:@{@"addressBook":testString, @"uuid":myUUID} block:^(id object, NSError *error) {
        if(!error){
            for (id c in object) {
                NSLog(@"%@ \n", c);
            }
            //NSLog(@"%@", object[0]);
            NSLog(@"success...?");
        }else{
            NSLog(@"findExistingUsers failed");
        }
    }];
}

- (void)testSendMessage{
    //NSString *senderID = @"710781c4-afb4-459e-a43c-561d2e92ebae"; // hemans phone
    NSString *senderID = @"F01EE22C-1B35-4EC7-9F4A-7DA5EE44832B"; // black ipod touch
    //NSString *receiverID = @"8ff7abd5-3ba0-4a85-b4b5-13a41bd7f260";
    
    //NSString *receiverID = @"df295dd8-b6c2-4fc5-b17c-47a0a21ec107";
    NSString *receiverID = @"3A977DEF-414E-4D66-A73B-DB092BE0FC8C"; // silver ipod touch
    
    NSString *message = @"Hhelp me..";
    NSString *myName = @"Ota Amaize";
    
    NSData *imageData = UIImagePNGRepresentation([self imageFromDiskWithFileName:@"ownpic.png"]);
    PFFile *imageFile = [PFFile fileWithName:@"ownpic.png" data:imageData];
    [imageFile saveInBackground];
    
    [PFCloud callFunctionInBackground:@"sendMessage" withParameters:@{@"sender":senderID, @"receiver":receiverID, @"senderName":myName} block:^(id object, NSError *error) {
        if(!error){
            NSLog(@"%@", object);
            object[@"pictureMessage"] = imageFile;
            [object saveInBackground];
            NSLog(@"sendMessage succeeded");
        }else{
            NSLog(@"sendMessage failed");
        }
    }];
}

- (void)testCreateUser{
    
    NSString *testUUID = @"F01EE22C-1B35-4EC7-9F4A-7DA5EE44832B";
    NSString *testFirst = @"Hannah";
    NSString *testLast = @"Slaughter";
    NSNumber *testGender = [NSNumber numberWithInt:1];
    //NSNumber *lat = [NSNumber numberWithDouble:37.4045335];
    //NSNumber *lon = [NSNumber numberWithDouble:37.4045335];
    //NSString *testPhone = @"555-555-5555";
    
    NSData *imageData = UIImagePNGRepresentation([self imageFromDiskWithFileName:@"ownpic.png"]);
    PFFile *imageFile = [PFFile fileWithName:@"ownpic.png" data:imageData];
    [imageFile saveInBackground];
    
    [PFCloud callFunctionInBackground:@"createUser"
                       withParameters:@{@"uuid":testUUID, @"gender":testGender, @"firstName":testFirst, @"lastName":testLast}
                                block:^(id object, NSError *error) {
                                    if(!error){
                                        //for (id m in object){
                                        object[@"picture"] = imageFile;
                                        [object saveInBackground];
                                        NSLog(@"%@ \n", object);
                                        //}
                                        NSLog(@"createUser success..");
                                    }else{
                                        NSLog(@"createUser failed..");
                                    }
                                    
                                }];
}

- (void)testBlock{
    NSString *testBlocker = @"F01EE22C-1B35-4EC7-9F4A-7DA5EE44832B";
    NSString *testBlockee = @"F01EE22C-XXXX-XXXX-XXXX-7DA5EE44832B";
    
    [PFCloud callFunctionInBackground:@"blockUser"
                       withParameters:@{@"blocker":testBlocker, @"blockee":testBlockee}
                                block:^(id object, NSError *error) {
                                    if(!error){
                                        NSLog(@"Block User success..");
                                    }else{
                                        NSLog(@"Block User failed..");
                                    }
                                    
                                }];
    
}

- (void)testGetMessage{
    
    NSString *messageId = @"WaCEG1XyYO";
    [PFCloud callFunctionInBackground:@"getMessage" withParameters:@{@"messageId":messageId} block:^(id object, NSError *error) {
        if(!error){
            //for (id m in object){
            NSLog(@"%@ \n", object);
            //}
            NSLog (@"succes...?");
        }else{
            NSLog(@"get message failed..");
        }
        
        
    }];
    
}

- (void)testGetUnreadMessages{
    
    NSString *receiverID = @"0536B9B2-B43C-476B-AF59-F21A8C156AF0";
    [PFCloud callFunctionInBackground:@"getUnreadMessagesv3" withParameters:@{@"uuid":receiverID} block:^(id object, NSError *error) {
        if(!error){
            //for (id m in object){
            NSLog(@"%@ \n", object);
            //}
            NSLog (@"succes...?");
        }else{
            NSLog(@"get unread messages failed..");
        }
        
        
    }];
    
}

- (void)testreadMessage{
    
    NSString *messageID = @"eagsyk4vpN";
    [PFCloud callFunctionInBackground:@"readMessage" withParameters:@{@"messageObjectId":messageID} block:^(id object, NSError *error) {
        if(!error){
            NSLog (@"succes...?");
        }else{
            NSLog(@"read message notification failed..");
        }
        
        
    }];
    
}

- (void)testGetNearByUser{ }

- (void)testUpdateLocation{
    if (nil == self.locationManager)
        self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    self.locationManager.distanceFilter = 500; // meters
    
    [self.locationManager startUpdatingLocation];
}


- (UIImage*)imageFromDiskWithFileName:(NSString*)fileName
{
    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", self.cacheDirectoryPath, fileName]];
    
    return image;
}



- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    // If it's a relatively recent event, turn off updates to save power.
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0) {
        // If the event is recent, do something with it.
        NSLog(@"latitude %+.6f, longitude %+.6f\n",
              location.coordinate.latitude,
              location.coordinate.longitude);
        
        NSNumber *lat = [NSNumber numberWithDouble:location.coordinate.latitude];
        NSNumber *lon = [NSNumber numberWithDouble:location.coordinate.longitude];
        
        //below is Test code for cloud code Update Location and get nearby Users
        
        
        NSString *uuid = @"2979DA64-31BA-4A02-8C32-4402ED126035";
        
        [PFCloud callFunctionInBackground:@"updateLocation" withParameters:@{@"latitude":lat, @"longitude":lon, @"uuid":uuid} block:^(id object, NSError *error) {
            if(!error){
                NSLog (@"succes...?");
            }else{
                NSLog(@"updateLocation notification failed..");
            }
            
            
        }];
        //*/
        
        ///*
        [PFCloud callFunctionInBackground:@"getNearbyUsers" withParameters:@{@"latitude":lat, @"longitude":lon, @"uuid":uuid} block:^(id object, NSError *error) {
            if(!error){
                for (id m in object){
                    NSLog(@"%@ \n", m);
                }
                NSLog (@"succes...?");
            }else{
                NSLog(@"get nearbyUsers failed..");
            }
            
            
        }];
        //*/
        
    }
}

- (void)getOwnImage{
    [PFCloud callFunctionInBackground:@"getImage"
                       withParameters:@{@"uuid":[[NSUserDefaults standardUserDefaults] objectForKey:@"kRadarUUIDKey"]}
                                block:^(id object, NSError *error) {
                                    if (!error && [object isKindOfClass:[NSArray class]]) {
                                        
                                        PFFile *theImage = object[0][@"picture"];
                                        
                                        [theImage getDataInBackgroundWithBlock:^(NSData *data, NSError *error){
                                            if(!error){
                                                UIImage *image = [UIImage imageWithData:data];
                                                [self loadAndSaveOwnImageToDiskFromImage:image];
                                                NSLog(@"own image retrieval success... !!");
                                            }
                                        }];
                                    }else{
                                        NSLog(@"get own image from Cloud Failed..");
                                    }
                                }];
    
}

- (void)testDoNotDisturb{
    NSString *myUUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"kRadarUUIDKey"];
    
    [PFCloud callFunctionInBackground:@"doNotDisturb" withParameters:@{@"uuid":myUUID} block:^(id object, NSError *error) {
        if (!error){
            NSLog(@"do not disturb set");
        }else{
            NSLog(@"do not disturb Failed...");
        }
    }];
}

- (void)testPhoneVerification{
    NSString *myUUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"kRadarUUIDKey"];
    [PFCloud callFunctionInBackground:@"testPhoneVerification" withParameters:@{@"uuid":myUUID} block:^(id object, NSError *error) {
        if (!error){
            NSLog(@"Status: %@", object);
        }else{
            NSLog(@"phone verify Failed...");
        }
    }];
}


- (void)loadAndSaveOwnImageToDiskFromImage:(UIImage*)image {
    
    [UIImagePNGRepresentation(image) writeToFile:[self.cacheDirectoryPath stringByAppendingPathComponent:@"ownpic.png"] options:NSAtomicWrite error:nil];
}

@end
