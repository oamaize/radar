//
//  RDRLocalUserFinderViewController.m
//  Radar
//
//  Created by Rollin Su on 3/19/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRLocalUserFinderViewController.h"
#import "UIView+RDRTheme.h"
#import "RDRMCManager.h"
#import "RDRUser.h"

#import <CoreLocation/CoreLocation.h>

@interface RDRLocalUserFinderViewController ()
@property (weak, nonatomic) IBOutlet UIButton *findLocalUsersButton;
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation RDRLocalUserFinderViewController

- (instancetype) initWithLocationManager:(CLLocationManager *)locationManager
{
    self = [super initWithNibName:@"RDRLocalUserFinderViewController" bundle:nil];
    if (self) {
        _locationManager = locationManager;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.findLocalUsersButton rdr_applyButtonTheme];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)onFindLocalUsersButtonTapped:(id)sender
{
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.locationManager.distanceFilter = 500; // meters
    [self.locationManager startUpdatingLocation];
    
    [[RDRMCManager sharedRDRMCManager] setupWithDisplayName:[RDRUser currentUser].phoneNumber];
    [[RDRMCManager sharedRDRMCManager] advertiseSelf:true];
    [[RDRMCManager sharedRDRMCManager] startBrowsing:true];
}

@end
