//
//  RDRMainHeaderView.m
//  Radar
//
//  Created by Rollin Su on 2/2/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRMainHeaderView.h"
#import "UIView+RDRTheme.h"
#import <QuartzCore/QuartzCore.h>
@interface RDRMainHeaderView()
@end

@implementation RDRMainHeaderView

- (instancetype)initWithFrame:(CGRect)frame type:(RDRMainHeaderViewType)type
{
    if(type == RDRMainHeaderViewTypeFriends) {
        self = [[UINib nibWithNibName:@"RDRMainHeaderView" bundle:nil] instantiateWithOwner:nil options:nil][0];
    } else if (type == RDRMainHeaderViewTypeLocal) {
        self = [[UINib nibWithNibName:@"RDRLocalHeaderView" bundle:nil] instantiateWithOwner:nil options:nil][0];
    } else {
        self = [[UINib nibWithNibName:@"RDRSettingsHeaderView" bundle:nil] instantiateWithOwner:nil options:nil][0];
    }
    if (self) {
        self.frame = frame;
        self.backgroundColor = [UIView rdr_primaryThemeColor];
    }
    return self;
}

@end
