//
//  RadarAppDelegate.h
//  Radar
//
//  Created by Ota-Benga Amaize on 11/1/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface RDRRadarAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableDictionary *viewControllers;


- (void)registerViewController:(NSString *)name controller:(UIViewController *)controller;

@end
