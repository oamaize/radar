//
//  UIView+RDRTheme.m
//  Radar
//
//  Created by Rollin Su on 2/24/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "UIView+RDRTheme.h"

@implementation UIView (RDRTheme)
+ (UIColor *)rdr_primaryThemeColor
{
    return [UIColor colorWithRed:62/255.0 green:135/255.0 blue:207/255.0 alpha:1.f];
}

- (void)rdr_applyButtonTheme
{
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 2.f;
    self.layer.cornerRadius = 5.f;
}

- (void)rdr_applyButtonLightTheme
{
    self.layer.borderColor = [UIView rdr_primaryThemeColor].CGColor;
    self.layer.borderWidth = 2.f;
    self.layer.cornerRadius = 5.f;
}

- (void)rdr_applyInactiveButtonTheme
{
    //self.layer.borderColor = [UIColor lightTextColor].CGColor;
    self.layer.borderColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:.30f].CGColor;
    self.layer.borderWidth = 2.f;
    self.layer.cornerRadius = 5.f;
}

- (void)rdr_applyMainBackgroundColor
{
    //62, 135, 207
    self.backgroundColor = [UIView rdr_primaryThemeColor];
}

- (void)rdr_applyShadow
{
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = .20f;
    self.layer.shadowOffset = CGSizeMake(3.f, 3.f);
    self.layer.shadowRadius = 2.f;
}

@end
