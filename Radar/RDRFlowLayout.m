//
//  RDRFlowLayout.m
//  Radar
//
//  Created by Rollin Su on 2/2/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRFlowLayout.h"

@implementation RDRFlowLayout

- (CGSize)collectionViewContentSize
{
    CGSize size = [super collectionViewContentSize];
    size.height = MAX(size.height, CGRectGetHeight(self.collectionView.bounds) + 44.f); //add the bar height so we can hide the bar initially
    return size;
}

@end
