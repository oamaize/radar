//
//  RDRMCManager.m
//  Radar
//
//  Created by Ota-Benga Amaize on 4/26/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRMCManager.h"
#import "RDRConstants.h"

@implementation RDRMCManager

+ (instancetype)sharedRDRMCManager
{
    static RDRMCManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self class] new];
    });
    return sharedInstance;
}

-(id)init{
    self = [super init];
    
    if (self) {
        _peerID = nil;
        //_session = nil;
        _browser = nil;
        _advertiser = nil;
    }
    
    return self;
}

#pragma mark - public methods

-(void)setupWithDisplayName:(NSString *)displayName{
    self.peerID = [[MCPeerID alloc] initWithDisplayName:displayName];
    
    if(self.peerID){
        self.advertiser = [[MCNearbyServiceAdvertiser alloc] initWithPeer:self.peerID discoveryInfo:nil serviceType:RDRServiceType];
        self.advertiser.delegate = self;
    
        self.browser = [[MCNearbyServiceBrowser alloc] initWithPeer:self.peerID serviceType:RDRServiceType];
        self.browser.delegate = self;
    }
    
}

-(void)advertiseSelf:(BOOL)shouldAdvertise{
    if(shouldAdvertise){
        [self.advertiser startAdvertisingPeer];
        NSLog(@"started advertising...");
    } else {
        [self.advertiser stopAdvertisingPeer];
        NSLog(@"stopped advertising...");
    }
}

-(void)startBrowsing:(BOOL)shouldBrowse{
    if(shouldBrowse){
        [self.browser startBrowsingForPeers];
        NSLog(@"started browsing...");

    } else {
        [self.browser stopBrowsingForPeers];
        NSLog(@"stopped browsing...");

    }
}


#pragma mark - delegate methods

-(void)browser:(MCNearbyServiceBrowser *)browser foundPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary *)info{
    
    
    NSLog(@"Found peer: %@", peerID.displayName);
    //notify mainviewcontroller to either pull this user's info into local view OR simply slap on a "nearby indicator" icon to the matching user in local view
}

-(void)browser:(MCNearbyServiceBrowser *)browser lostPeer:(MCPeerID *)peerID{
    
}

-(void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID withContext:(NSData *)context invitationHandler:(void (^)(BOOL, MCSession *))invitationHandler{
    
}

@end
