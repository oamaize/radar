//
//  RDRPhotoCaptureViewController.m
//  Radar
//
//  Created by Rollin Su on 11/12/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRPhotoCaptureViewController.h"

#import "UIView+RDRTheme.h"

@interface RDRPhotoCaptureViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIButton *uploadPictureButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation RDRPhotoCaptureViewController

- (id)init
{
    self = [super initWithNibName:@"RDRPhotoCaptureViewController" bundle:nil];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.uploadPictureButton rdr_applyButtonTheme];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onUploadPictureButtonTapped:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Picture",@"Choose Existing", nil];
        
        UIViewController *parentVC = self;
        while (parentVC.parentViewController) {
            parentVC = parentVC.parentViewController;
        }
        
        [actionSheet showInView:parentVC.view];
        
    } else {
        UIImagePickerController *picker = [UIImagePickerController new];
        picker.delegate = self;
        picker.allowsEditing = YES;
        
        [self.parentViewController presentViewController:picker animated:YES completion:nil];
    }
}

#pragma mark - UIImagePickerControllerDelegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    self.imageView.image = image;
    [self.imageView rdr_applyButtonTheme];
    [self.uploadPictureButton setTitle:@"Edit" forState:UIControlStateNormal];
    [self.delegate photoCaptureViewControllerDidCapturePhoto:self photo:image];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate Methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0) {
        UIImagePickerController *mediaPicker = [[UIImagePickerController alloc] init];
        [mediaPicker setDelegate:self];
        mediaPicker.allowsEditing = YES;
        mediaPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        mediaPicker.cameraDevice=UIImagePickerControllerCameraDeviceFront;
        [self presentViewController:mediaPicker animated:YES completion:nil];
        
    } else if(buttonIndex == 1) {
        UIImagePickerController *mediaPicker = [[UIImagePickerController alloc] init];
        [mediaPicker setDelegate:self];
        mediaPicker.allowsEditing = YES;
        mediaPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:mediaPicker animated:YES completion:nil];
        
    }
}


@end
