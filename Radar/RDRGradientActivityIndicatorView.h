//
//  RDRGradientActivityIndicatorView.h
//  Radar
//
//  Created by Rollin Su on 2/4/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDRGradientActivityIndicatorView : UIView

@property (nonatomic, strong, readonly) CALayer *gradientLayer;
@property (nonatomic, assign, getter = isAutoReverse) BOOL autoReverse;

- (void)startAnimating;
- (void)stopAnimating;

@end
