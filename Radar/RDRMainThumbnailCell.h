//
//  RDRMainThumbnailCell.h
//  Radar
//
//  Created by Rollin Su on 11/15/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, RDRMainThumbnailCellBorder) {
    RDRMainThumbnailCellBorderNone      = 0,
    RDRMainThumbnailCellBorderTop       = 1 << 0,
    RDRMainThumbnailCellBorderLeft      = 1 << 1,
    RDRMainThumbnailCellBorderBottom    = 1 << 2,
    RDRMainThumbnailCellBorderRight     = 1 << 3,
};

@class JSBadgeView;
@class RDRGradientActivityIndicatorView;
@interface RDRMainThumbnailCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (assign, nonatomic) BOOL isWaitingForImage;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic, readonly) JSBadgeView *badgeView;
@property (weak, nonatomic) IBOutlet RDRGradientActivityIndicatorView *gradientIndicatorView;
@property (assign, nonatomic) RDRMainThumbnailCellBorder border;

@end
