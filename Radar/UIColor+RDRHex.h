//
//  UIColor+UIColor_RDRHex.h
//  Radar
//
//  Created by Rollin Su on 12/7/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RDRHex)

+ (UIColor*)rdr_colorWithHexLiteral:(NSUInteger)hexLiteral;

@end
