//
//  RDRMainPageViewController.m
//  Radar
//
//  Created by Rollin Su on 2/27/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRMainPageViewController.h"
#import "RDRSignUpViewController.h"
#import "RDRMainViewController.h"
#import "RDRUser.h"
#import "UIView+RDRTheme.h"
#import "RDRLocalUserFinderViewController.h"
#import "RDRDoNotDisturbViewController.h"
#import "RDRConnectDeviceViewController.h"
#import "RDRAddressBookController.h"
#import "RDRMCManager.h"

#import <AddressBook/AddressBook.h>
#import <Parse/Parse.h>
#import <UIAlertView-Blocks/UIAlertView+Blocks.h>
#import <UIAlertView-Blocks/UIActionSheet+Blocks.h>

@interface RDRMainPageViewController () <UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) RDRMainViewController *friendsViewController;
@property (strong, nonatomic) RDRMainViewController *localViewController;
@property (weak, nonatomic) IBOutlet UIButton *importContactsButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *panGestureRecognizer;
@property (assign, nonatomic) CGFloat currentX;
@property (weak, nonatomic) RDRMainViewController *currentViewController;
@property (weak, nonatomic) IBOutlet UIButton *findMyFriendsButton;
@end

@implementation RDRMainPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if (![RDRUser currentUser]) {
        RDRUser *user = [RDRUser userFromDisk];
        if (!user) {
            [self presentViewController:[RDRSignUpViewController new] animated:NO completion:nil];
        } else {
            [RDRUser setCurrentUser:user];
            [[RDRMCManager sharedRDRMCManager] setupWithDisplayName:user.phoneNumber];
            [[RDRMCManager sharedRDRMCManager] advertiseSelf:true];
            [[RDRMCManager sharedRDRMCManager] startBrowsing:true];
        }
    }

    self.friendsViewController = [[RDRMainViewController alloc] initWithType:RDRMainViewControllerTypeFriends];
    self.localViewController = [[RDRMainViewController alloc] initWithType:RDRMainViewControllerTypeLocal];
    
    
    [self.importContactsButton rdr_applyButtonTheme];
    [self.view rdr_applyMainBackgroundColor];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeySkipFindFriendsButton]) {
        NSAssert([RDRUser currentUser], @"No user loaded. Shouldn't get here when skip find-friends is true");
        self.findMyFriendsButton.hidden = YES;
        //[self.spinner startAnimating];
        [PFCloud callFunctionInBackground:@"getSyncStatus" withParameters:@{@"phoneNumber": [RDRUser currentUser].phoneNumber} block:^(NSString *object, NSError *error) {
            if (!error) {
                if([object isEqualToString:@"true"]) {
                    NSLog(@"need to sync addressbook..");
                    [[RDRAddressBookController sharedAddressBookController] syncAddressBookOnComplete:^{
                        [self loadMainViewController];
                    }];
                } else {
                    NSLog(@"no need to sync addressbook..");
                    [self loadMainViewController];
                }
            } else {
                NSLog(@"Get Sync Status Error: %@", error);
                [self loadMainViewController];
            }
        }];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onImportContactsButtonTapped:(id)sender
{
    [self.spinner startAnimating];
    [self getAddressBookPhoneNumbers];
}

- (void)getAddressBookPhoneNumbers
{
    // Request authorization to Address Book
    NSMutableArray *numbers = [NSMutableArray new];
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
                for(CFIndex i = 0; i < CFArrayGetCount(people); i++){
                    ABRecordRef record = CFArrayGetValueAtIndex(people, i);
                    ABMultiValueRef phoneNumbers = ABRecordCopyValue(record, kABPersonPhoneProperty);
                    NSString *label;
                    for(CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++){
                        label = (__bridge NSString *)(ABMultiValueCopyLabelAtIndex(phoneNumbers, i));
                        if([label isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        } else if ([label isEqualToString:(NSString *)kABPersonPhoneMobileLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        } else if([label isEqualToString:(NSString *)kABPersonPhoneMainLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        }else if([label isEqualToString:(NSString *)kABHomeLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        }else if([label isEqualToString:(NSString *)kABWorkLabel]){
                            [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                        }
                    }
                }
                [self processRawPhoneNumbers:numbers];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Please go to Settings > Privacy > Contacts to give Radar access to your contacts." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                [alert show];
            }
        });
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
        for(CFIndex i = 0; i < CFArrayGetCount(people); i++){
            ABRecordRef record = CFArrayGetValueAtIndex(people, i);
            ABMultiValueRef phoneNumbers = ABRecordCopyValue(record, kABPersonPhoneProperty);
            NSString *label;
            for(CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++){
                label = (__bridge NSString *)(ABMultiValueCopyLabelAtIndex(phoneNumbers, i));
                if([label isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }else if([label isEqualToString:(NSString *)kABPersonPhoneMobileLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }else if([label isEqualToString:(NSString *)kABPersonPhoneMainLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }else if([label isEqualToString:(NSString *)kABHomeLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }else if([label isEqualToString:(NSString *)kABWorkLabel]){
                    [numbers addObject:(__bridge NSString*)(ABMultiValueCopyValueAtIndex(phoneNumbers, i))];
                }
            }
        }
        [self processRawPhoneNumbers:numbers];
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Please go to Settings > Privacy > Contacts to give Radar access to your contacts." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"We can't access your contacts. You may have parental control enabled" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)processRawPhoneNumbers:(NSArray *)phonenNumbers
{
    NSString *normalizedPhoneNumberes = [self normalizedPhoneNumbers:phonenNumbers];
    [self getExistingUsersOnRadarWithPhoneNumbers:normalizedPhoneNumberes];
}

- (NSString *)normalizedPhoneNumbers:(NSArray *)phoneNumbers
{
    NSMutableArray *normalizedNumbers = [NSMutableArray new];
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"+- ()\u00a0"];
    
    for (NSString *n in phoneNumbers) {
        NSString *foo = [[n componentsSeparatedByCharactersInSet:charSet] componentsJoinedByString:@""];
        if(foo.length == 11){ //dropping US country code from 11 digit numbers
            foo = [foo substringFromIndex:1]; //TODO: handle intl' numbers
        }
        [normalizedNumbers addObject:foo];
    }
    
    return [normalizedNumbers componentsJoinedByString:@","];
}

- (void)getExistingUsersOnRadarWithPhoneNumbers:(NSString *) phoneNumbers{
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:RDRDefaultsKeySkipFindFriendsButton];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (phoneNumbers.length == 0) {
        NSLog(@"Contact list is empty");
        [self loadMainViewController];
        return;
    }
    NSString *myPhoneNumber = [RDRUser currentUser].phoneNumber;
    NSString *myName = [RDRUser currentUser].fullName;
    
    [PFCloud callFunctionInBackground:@"findExistingUsers" withParameters:@{@"addressBook":phoneNumbers, @"phoneNumber":myPhoneNumber, @"myName":myName} block:^(id object, NSError *error) {
        if(!error) {
            if ([object count] == 0) {
                [[[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Looks like none of your friends have joined Radar yet. Invite someone!"
                                   cancelButtonItem:[RIButtonItem itemWithLabel:@"Invite" action:^{
                    NSString *shareString = @"Hurry, come join me on Radar! It's the simplest way for us to stay connected :)";
                    NSURL *shareUrl = [NSURL URLWithString:@"http://tryradar.com"];
                    
                    NSArray *activityItems = [NSArray arrayWithObjects:shareString, shareUrl, nil];
                    
                    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
                    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                    
                    [self presentViewController:activityViewController animated:YES completion:nil];
                    [self loadMainViewController];
                }]
                                   otherButtonItems:[RIButtonItem itemWithLabel:@"Cancel" action:^{
                    [self loadMainViewController];
                }], nil] show];
            } else {
                [self saveFriendsListToDisk:object];
                [[NSNotificationCenter defaultCenter] postNotificationName:RDRNotificationFriendsListUpdated object:self];
                [self loadMainViewController];
            }
        } else {
            NSLog(@"findExistingUsers failed");
        }
    }];
    
}

- (void)saveFriendsListToDisk:(NSArray *)friends
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    path = [path stringByAppendingPathComponent:@"friends.plist"];
    
    if(![friends writeToFile:path atomically:YES]) {
        NSLog(@"Write friends file failed..");
    }
    
}

- (void)loadMainViewController
{
    [self addChildViewController:self.friendsViewController];
    [self addChildViewController:self.localViewController];
    [self.view addSubview:self.friendsViewController.view];
    
    CGRect frame = self.localViewController.view.frame;
    frame.origin.x = frame.size.width;
    self.localViewController.view.frame = frame;
    [self.view addSubview:self.localViewController.view];
    self.currentViewController = self.friendsViewController;
    [self.currentViewController didBecomeCurrentViewController];
    
    NSInteger hintShownCount = [[NSUserDefaults standardUserDefaults] integerForKey:RDRDefaultsKeyHintShowCount];
    if (hintShownCount < 2) {
        [[NSUserDefaults standardUserDefaults] setInteger:++hintShownCount forKey:RDRDefaultsKeyHintShowCount];
        self.view.userInteractionEnabled = NO;
        CGRect originalFriendFrame = self.friendsViewController.view.frame;
        CGRect originalLocalFrame = self.localViewController.view.frame;
        
        
        [UIView animateWithDuration:0.4f delay:.7f options:kNilOptions animations:^{
            
            CGRect frame = self.friendsViewController.view.frame;
            frame.origin.x = -50.f;
            self.friendsViewController.view.frame = frame;
            
            frame = self.localViewController.view.frame;
            frame.origin.x = 270.f;
            
            self.localViewController.view.frame = frame;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.4f delay:0.f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                self.friendsViewController.view.frame = originalFriendFrame;
                self.localViewController.view.frame = originalLocalFrame;
            } completion:^(BOOL finished) {
                self.view.userInteractionEnabled = YES;
            }];
        }];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyIsDisconnected]) {
        RDRConnectDeviceViewController *connectDeviceViewController = [RDRConnectDeviceViewController new];
        [self addChildViewController:connectDeviceViewController];
        [self.view addSubview:connectDeviceViewController.view];
    }
}

#pragma mark - Public Methods

- (void)disconnectOnComplete:(void (^)())onComplete
{
    [self.spinner stopAnimating];
    [self presentViewController:[RDRSignUpViewController new] animated:YES completion:^{
        [self.friendsViewController.view removeFromSuperview];
        [self.friendsViewController removeFromParentViewController];
        
        [self.localViewController.view removeFromSuperview];
        [self.localViewController removeFromParentViewController];
        if (onComplete) {
            onComplete();
        }
        self.friendsViewController = [[RDRMainViewController alloc] initWithType:RDRMainViewControllerTypeFriends];
        self.localViewController = [[RDRMainViewController alloc] initWithType:RDRMainViewControllerTypeLocal];
    }];
}

#pragma mark - Gesture Recognizer
- (IBAction)onPan:(UIPanGestureRecognizer *)gesture {
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            self.currentX = self.friendsViewController.view.frame.origin.x;
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGFloat translationX = [gesture translationInView:self.view].x;
            
            CGRect frame = self.friendsViewController.view.frame;
            frame.origin.x = MIN(0, MAX(-320.f, self.currentX + translationX));
            self.friendsViewController.view.frame = frame;
            
            frame.origin.x += 320.f;
            self.localViewController.view.frame = frame;
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        {
            CGFloat velocityX = [gesture velocityInView:self.view].x;
            [UIView animateWithDuration:0.2f animations:^{
                CGRect frame = self.friendsViewController.view.frame;
                if (velocityX < -680.f) {
                    frame.origin.x = -320.f;
                } else if (velocityX > 680.f) {
                    frame.origin.x = 0.f;
                } else {
                    frame.origin.x = frame.origin.x < -160.f ? -320.f : 0.f;
                }
                self.friendsViewController.view.frame = frame;
                
                if (frame.origin.x == 0.f) {
                    self.currentViewController = self.friendsViewController;
                } else {
                    self.currentViewController = self.localViewController;
                }
                [self.currentViewController didBecomeCurrentViewController];
                
                frame.origin.x += 320.f;
                self.localViewController.view.frame = frame;
                
            }];
        }
            break;
        default:
            break;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (self.currentViewController == self.friendsViewController && self.friendsViewController.childViewControllers.count > 0) {
        return NO;
    } else if (self.currentViewController == self.localViewController) {
        if (self.localViewController.childViewControllers.count == 0) {
            return YES;
        } else if ([self.localViewController.childViewControllers.lastObject isKindOfClass:[RDRLocalUserFinderViewController class]]) {
            return YES;
        } else if ([self.localViewController.childViewControllers.lastObject isKindOfClass:[RDRDoNotDisturbViewController class]]) {
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (self.panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        return NO;
    }
    return NO;
    //return ([otherGestureRecognizer.view isKindOfClass:[UICollectionView class]]);
}
            
@end
