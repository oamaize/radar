//
//  RDRSlideUpViewController.m
//  Radar
//
//  Created by Rollin Su on 1/16/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRSlideUpViewController.h"
#import "UIImage+RDRBlur.h"
#import "UIView+RDRUIImage.h"

@interface RDRSlideUpViewController () <UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *panGesture;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (weak, nonatomic) UIView *blurView;
@end

@implementation UIViewController(RDRSlideUpViewController)

- (void)rdr_slideupViewController:(UIViewController *)vc
{
    RDRSlideUpViewController *slider = [[RDRSlideUpViewController alloc] initWithNibName:@"RDRSlideUpViewController" bundle:nil];
    
    UIImage *blurredImage = [[self.view rdr_image] rdr_blurredImageWithRadius:0.3f];
    UIImageView *blurredImageView = [[UIImageView alloc] initWithImage:blurredImage];
    [self.view addSubview:blurredImageView];
    blurredImageView.alpha = 0.f;
    
    [self addChildViewController:slider];
    [self.view addSubview:slider.view];
    slider.view.frame = self.view.bounds;
    
    slider.blurView = blurredImageView;
    
    if (vc) {
        [slider addChildViewController:vc];
        [slider.containerView addSubview:vc.view];
        vc.view.frame = slider.containerView.bounds;
    }
    
    
    CGRect __block frame = slider.view.frame;
    frame.origin.y = self.view.bounds.size.height;
    slider.view.frame = frame;
    [UIView animateWithDuration:0.3f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        frame.origin.y = 0.f;
        slider.view.frame = frame;
        blurredImageView.alpha = 1.f;
    } completion:^(BOOL finished) {
    }];
}

@end






@implementation RDRSlideUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onCloseTapped:(id)sender {
    [UIView animateWithDuration:0.3f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = self.view.superview.bounds.size.height;
        self.view.frame = frame;
        self.blurView.alpha = 0.f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        [self.blurView removeFromSuperview];
    }];
}

- (IBAction)onPan:(UIPanGestureRecognizer *)pan
{
    if (pan.state == UIGestureRecognizerStateBegan) {
        
    } else if (pan.state == UIGestureRecognizerStateChanged) {
        CGFloat deltay = [pan translationInView:self.view.superview].y;
        if(deltay < 0) return;
        CGRect frame = self.view.frame;
        frame.origin = CGPointMake(0.f, deltay);
        self.view.frame = frame;
        self.blurView.alpha = 1 - (deltay/(self.view.bounds.size.height-[pan locationInView:self.view].y));
    } else if (pan.state == UIGestureRecognizerStateEnded || pan.state == UIGestureRecognizerStateCancelled) {
        if ([pan translationInView:self.view.superview].y > self.view.frame.size.height/3.f ||
            [pan velocityInView:self.view.superview].y > 600.f) {
            [self onCloseTapped:self];
        } else {
            [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                CGRect frame = self.view.frame;
                frame.origin = CGPointZero;
                self.view.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
        }
    }
}

#pragma mark - Gesture recognizer
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return (touch.view == self.view);
}

@end
