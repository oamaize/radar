//
//  RDRPopUpViewController.m
//  Radar
//
//  Created by Rollin Su on 1/23/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRPopUpViewController.h"
#import "RDRSendChatViewController.h"
#import "UIImage+RDRBlur.h"
#import "UIView+RDRUIImage.h"

@interface RDRPopUpViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) UIView *blurredImageView;
@property (weak, nonatomic) UIView *screenCapImageView;
@property (copy, nonatomic) void (^onDismissHandler)();
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *panGestureRecognizer;
@end

@implementation RDRPopUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissLeft:(BOOL)left
{
    [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGRect frame = self.containerView.frame;
        frame.origin.x = self.containerView.frame.size.width;
        if (left) {
            frame.origin.x *= -1;
        }
        self.containerView.frame = frame;
        self.blurredImageView.alpha = 0.f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        [self.blurredImageView removeFromSuperview];
        if (self.onDismissHandler) {
            self.onDismissHandler();
        }
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.3f animations:^{
        CGRect frame = self.containerView.frame;
        frame.origin.y = self.containerView.bounds.size.height;
        self.containerView.frame = frame;
        self.blurredImageView.alpha = 0.f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        [self.blurredImageView removeFromSuperview];
        if (self.onDismissHandler) {
            self.onDismissHandler();
        }
    }];
}

- (IBAction)onPan:(UIPanGestureRecognizer *)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGFloat deltaX = [sender translationInView:self.view.superview].x;
            
            CGRect frame = self.containerView.frame;
            frame.origin = CGPointMake(20 + deltaX, frame.origin.y);
            self.containerView.frame = frame;
            self.blurredImageView.alpha = 1 - (fabsf(deltaX)/(self.view.bounds.size.width));
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            
            CGFloat deltaX = [sender translationInView:self.view.superview].x;
            if (deltaX > 100) {
                [self dismissLeft:NO];
            } else if (deltaX < -100) {
                [self dismissLeft:YES];
            } else {
                [UIView animateWithDuration:0.3f delay:0.f usingSpringWithDamping:0.7f initialSpringVelocity:0.2f options:kNilOptions animations:^{
                    self.blurredImageView.alpha = 1.f;
                    self.containerView.frame = self.view.bounds;
                } completion:nil];
            }
        }
            break;
        default:
            break;
    }
}

@end

@implementation UIViewController (RDRPopUpViewController)

- (void)rdr_popUpViewController:(UIViewController *)vc fromRect:(CGRect)rect enablePanDismiss:(BOOL)enablePanDismiss onComplete:(void (^)())onCompleteHandler onDismiss:(void (^)())onDismissHandler
{
    
    //Blur image
    UIImage *screenCapImage = [self.view rdr_image];
    UIImage *blurredImage = [screenCapImage rdr_blurredImageWithRadius:.3f];
    UIImageView *blurredImageView = [[UIImageView alloc] initWithImage:blurredImage];
    blurredImageView.alpha = 0.f;
    
    RDRPopUpViewController *popupVC = [[RDRPopUpViewController alloc] initWithNibName:@"RDRPopUpViewController" bundle:nil];
    popupVC.onDismissHandler = onDismissHandler;
    popupVC.view.frame = self.view.bounds;
    
    popupVC.panGestureRecognizer.enabled = enablePanDismiss;
    
    [popupVC.view insertSubview:blurredImageView atIndex:0];
    popupVC.blurredImageView = blurredImageView;
    
    // Make the input view controller the child of popup view controller
    if (vc) {
        [popupVC addChildViewController:vc];
        [popupVC.containerView addSubview:vc.view];
        vc.view.frame = popupVC.containerView.bounds;
    }
    
    // Make popup view controller the child of the receiver
    [self addChildViewController:popupVC];
    [self.view addSubview:popupVC.view];
    
    
    // Animate in
    popupVC.containerView.frame = rect;
    [UIView animateWithDuration:0.7f delay:0.f usingSpringWithDamping:0.5f initialSpringVelocity:0.2 options:kNilOptions animations:^{
        popupVC.containerView.frame = CGRectInset(popupVC.view.bounds, 20.f, 20.f);
        blurredImageView.alpha = 1.f;
    } completion:^(BOOL finished) {
        if (onCompleteHandler) {
            onCompleteHandler();
        }
    }];
}


- (void)rdr_dismissPopUpViewController
{
    // Try to model the iOS modal dimiss behavior
    if ([self isKindOfClass:[RDRPopUpViewController class]]) {
        [(RDRPopUpViewController*)self dismiss];
        return;
    }
    for (UIViewController *vc in self.childViewControllers) {
        if ([vc isKindOfClass:[RDRPopUpViewController class]]) {
            [((RDRPopUpViewController *)vc) dismiss];
            return;
        }
    }
    [self.parentViewController rdr_dismissPopUpViewController];
}

@end
