//
//  RDRDoNoDistrubViewController.m
//  Radar
//
//  Created by Rollin Su on 4/12/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRDoNotDisturbViewController.h"
#import "UIView+RDRTheme.h"
#import <Parse/Parse.h>
#import "RDRUser.h"

@interface RDRDoNotDisturbViewController ()
@property (weak, nonatomic) IBOutlet UIButton *turnOffButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation RDRDoNotDisturbViewController

- (instancetype)init
{
    self = [super initWithNibName:@"RDRDoNotDisturbViewController" bundle:nil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.turnOffButton rdr_applyButtonTheme];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTurnOffButtonTapped:(id)sender
{
    self.turnOffButton.hidden = YES;
    [self.spinner startAnimating];
    [PFCloud callFunctionInBackground:@"doNotDisturb" withParameters:@{@"phoneNumber":[RDRUser currentUser].phoneNumber} block:^(id object, NSError *error) {
        if (!error) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:RDRDefaultsKeyDoNotDisturb];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Oops" message:@"We're unable to set your DND status at the moment. Please try again" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
        }
        self.turnOffButton.hidden = NO;
        [self.spinner stopAnimating];
    }];
}
@end
