//
//  RDRPhotoCaptureViewController.h
//  Radar
//
//  Created by Rollin Su on 11/12/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RDRPhotoCaptureViewControllerDelegate;
@interface RDRPhotoCaptureViewController : UIViewController
@property (nonatomic, weak) id<RDRPhotoCaptureViewControllerDelegate> delegate;
@end



@protocol RDRPhotoCaptureViewControllerDelegate <NSObject>

@optional
- (void)photoCaptureViewControllerDidCancel:(RDRPhotoCaptureViewController*)photoCaptureViewController;
- (void)photoCaptureViewControllerDidCapturePhoto:(RDRPhotoCaptureViewController*)photoCaptureViewController photo:(UIImage*) image;

@end