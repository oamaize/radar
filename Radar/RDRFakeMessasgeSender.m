//
//  RDRFakeMessasgeSender.m
//  Radar
//
//  Created by Rollin Su on 3/5/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRFakeMessasgeSender.h"

#import "RDRUser.h"

@interface RDRFakeMessasgeSender()
@property (nonatomic, strong) NSMapTable *timers;
@end

@implementation RDRFakeMessasgeSender

- (instancetype)init
{
    self = [super init];
    if (self) {
        _timers = [[NSMapTable alloc] initWithKeyOptions:NSPointerFunctionsStrongMemory valueOptions:NSPointerFunctionsWeakMemory capacity:3];
    }
    return self;
}

- (void)dealloc
{
    [self stopAllMessages];
}

- (void)startFakeMessagesFromUser:(RDRUser *)user timeInterval:(NSTimeInterval)interval
{
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(onTimerFired:) userInfo:user.phoneNumber repeats:YES];
    [self.timers setObject:timer forKey:user];
}

- (void)stopFakeMessagesFromUser:(RDRUser *)user
{
    NSTimer *timer = [self.timers objectForKey:user];
    [timer invalidate];
    [self.timers removeObjectForKey:user];
}

- (void)stopAllMessages
{
    for (RDRUser *user in self.timers.keyEnumerator) {
        [[self.timers objectForKey:user] invalidate];
    }
    [self.timers removeAllObjects];
}

#pragma mark - Timer
- (void)onTimerFired:(NSTimer *)timer
{
    static NSUInteger messageId = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:RDRNotificationIncomingMessage object:self userInfo:@{@"sender": timer.userInfo,
                                                                                                                     @"message": @"Hey wassup?",
                                                                                                                     @"messageId": @(messageId++).stringValue}];
}

@end
