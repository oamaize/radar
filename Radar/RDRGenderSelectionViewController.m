//
//  RDRGenderSelectionViewController.m
//  Radar
//
//  Created by Rollin Su on 11/20/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRGenderSelectionViewController.h"
#import "RDRUser.h"

@interface RDRGenderSelectionViewController ()
@property (weak, nonatomic) IBOutlet UIButton *guyButton;
@property (weak, nonatomic) IBOutlet UIButton *girlButton;


@end

@implementation RDRGenderSelectionViewController

- (id)init
{
    self = [super initWithNibName:@"RDRGenderSelectionViewController" bundle:nil];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onGuyButtonTapped:(UIButton*)btn {
    [btn setImage:[UIImage imageNamed:@"male_100"] forState:UIControlStateNormal];
    [self.girlButton setImage:[UIImage imageNamed:@"female_50"] forState:UIControlStateNormal];
    [self.delegate genderController:self didSelectUserGender:RDRUserGenderMale];
}

- (IBAction)onGirlButtonTapped:(UIButton*)btn {
    [btn setImage:[UIImage imageNamed:@"female_100"] forState:UIControlStateNormal];
    [self.guyButton setImage:[UIImage imageNamed:@"male_50"] forState:UIControlStateNormal];
    [self.delegate genderController:self didSelectUserGender:RDRUserGenderFemale];
}


@end
