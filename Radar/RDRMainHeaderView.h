//
//  RDRMainHeaderView.h
//  Radar
//
//  Created by Rollin Su on 2/2/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, RDRMainHeaderViewType) {
    RDRMainHeaderViewTypeFriends,
    RDRMainHeaderViewTypeLocal,
    RDRMainHeaderViewTypeSettings
};

@interface RDRMainHeaderView : UIView

// Friends outlet
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

// Local outlets
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *filterControl;

// Settings outlet
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (instancetype)initWithFrame:(CGRect)frame type:(RDRMainHeaderViewType)type;

@end
