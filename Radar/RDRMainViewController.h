//
//  RDRMainViewController.h
//  Radar
//
//  Created by Rollin Su on 11/13/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

typedef NS_ENUM(NSInteger, RDRMainViewControllerType) {
    RDRMainViewControllerTypeFriends,
    RDRMainViewControllerTypeLocal
};

@interface RDRMainViewController : UIViewController

@property (nonatomic, assign, readonly) RDRMainViewControllerType type;

- (instancetype) initWithType:(RDRMainViewControllerType)type;

- (void)didBecomeCurrentViewController;

@end
