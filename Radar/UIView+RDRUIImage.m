//
//  UIView+RDRUIImage.m
//  Radar
//
//  Created by Rollin Su on 1/22/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "UIView+RDRUIImage.h"

@implementation UIView (RDRUIImage)

- (UIImage*)rdr_image
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultingImage;
}

@end
