//
//  UIImage+RDRRoundedImage.h
//  Radar
//
//  Created by Ota-Benga Amaize on 1/20/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (RDRRoundedImage)

+ (UIImage*)rdr_roundedImage:(UIImage *)image;

@end
