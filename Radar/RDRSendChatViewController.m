//
//  RDRSendChatViewController.m
//  Radar
//
//  Created by Ota-Benga Amaize on 2/20/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRSendChatViewController.h"
#import "RDRUser.h"
#import "RDRGradientActivityIndicatorView.h"
#import "UIView+RDRTheme.h"

#import <QuartzCore/QuartzCore.h>
#import <Parse/Parse.h>
#import <UIAlertView-Blocks/UIActionSheet+Blocks.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/AFNetworking.h>

@interface RDRSendChatViewController () <UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong, readwrite) RDRUser *remoteUser;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (nonatomic, strong) IBOutlet UIImageView *receiverImageView;
@property (nonatomic, strong) IBOutlet UILabel *receiverName;
@property (nonatomic, strong) IBOutlet UILabel *sendMessageLabel;
@property (nonatomic, strong) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) CAGradientLayer *maskLayer;
@property (weak, nonatomic) IBOutlet RDRGradientActivityIndicatorView *gradientView;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton2;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (assign, nonatomic) BOOL enableCamera;
@property (weak, nonatomic) IBOutlet UIButton *reportButton;
@end

@implementation RDRSendChatViewController

- (instancetype)initWithRemoteUser:(RDRUser *)user enableCamera:(BOOL)enableCamera {
    self = [super initWithNibName:@"RDRSendChatViewController" bundle:nil];
    if(self){
        _remoteUser = user;
        _enableCamera = enableCamera;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor clearColor];
    
    
    self.receiverName.text = _remoteUser.fullName;
    [self roundThemViews];
    
    [self.receiverImageView setImageWithURL:[NSURL URLWithString:_remoteUser.imageURLString]];

    UIImage *image = [UIImage imageNamed:@"send_button"];
    CALayer *maskLayer = [CALayer layer];
    maskLayer.frame = CGRectMake(0.f,0.f,image.size.width, image.size.height);
    maskLayer.contents = (__bridge id)image.CGImage;
    self.gradientView.frame = self.nextButton.frame;
    self.gradientView.gradientLayer.mask = maskLayer;
    self.gradientView.autoReverse = NO;
    [self.messageTextView becomeFirstResponder];
    self.cameraButton2.hidden = YES;
    self.cameraButton.hidden = !_enableCamera;
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGRect frame = self.progressView.frame;
    frame.size.width = 0.f;
    self.progressView.frame = frame;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)roundThemViews
{
    self.receiverImageView.layer.cornerRadius =
    self.shadowView.layer.cornerRadius = self.receiverImageView.bounds.size.width / 2.f;
    self.receiverImageView.clipsToBounds = YES;
    
    
    [self.shadowView rdr_applyShadow];
}

#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *str = [textView.text stringByReplacingCharactersInRange:range withString:text];
    return str.length <= 200;
}

- (void)textViewDidChange:(UITextView *)textView
{
    BOOL haveText = textView.text.length > 0;
    [self enableNextButton:haveText];
    self.cameraButton.enabled = self.reportButton.enabled = !haveText;
    self.sendMessageLabel.hidden = haveText;
    
}

#pragma mark - Button Actions
- (IBAction)onReportButtonTapped:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil cancelButtonItem:[RIButtonItem itemWithLabel:@"Cancel" action:nil] destructiveButtonItem:[RIButtonItem itemWithLabel:@"Report User" action:^{
        
        [self.messageTextView resignFirstResponder];
        if([MFMailComposeViewController canSendMail]){
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            [mailer setSubject:@"Report User Abuse"];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"team@tryradar.com", nil];
            [mailer setToRecipients:toRecipients];
            NSString *emailBody = @"Please describe the reason for this abuse incident report. We will investigate and get back to you as soon as possible! \n\n Best, \n\nTeam Radar";
            [mailer setMessageBody:emailBody isHTML:NO];
            
            [self presentViewController:mailer animated:YES completion:nil];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        }
    }] otherButtonItems:nil];
    
    [actionSheet showInView:self.view];
}

- (IBAction)onCancelButtonTapped:(id)sender
{
    [self.delegate sendChatViewControllerDidTapOnCloseButton:self];
}

- (IBAction)onNextButtonTapped:(id)sender
{
    [self.messageTextView resignFirstResponder];
    
    if (self.messageTextView.text.length > 0) {
        [UIView animateWithDuration:0.1f delay:0.f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveLinear animations:^{
            CGRect frame = self.progressView.frame;
            frame.size.width = self.view.bounds.size.width * .85f;
            self.progressView.frame = frame;
        } completion:^(BOOL finished) {
        }];
        [PFCloud callFunctionInBackground:@"sendMessage" withParameters:
         @{@"sender":[RDRUser currentUser].phoneNumber ? : @"1234",
           @"receiver":_remoteUser.phoneNumber, @"senderName":[RDRUser currentUser].fullName ? : @"Test Sender",
           @"message":self.messageTextView.text, @"receiverType":[NSNumber numberWithInt:_remoteUser.userType]}
                                    block:^(id object, NSError *error) {
                                        if(!error){
                                            NSLog(@"%@", object);
                                        }else{
                                            NSLog(@"sendMessage failed");
                                        }
                                    }];
        [UIView animateWithDuration:0.1f delay:0.f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveLinear animations:^{
            CGRect frame = self.progressView.frame;
            frame.size.width = self.view.bounds.size.width;
            self.progressView.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
    } else if (self.imageView.image) {
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.imageView.image.size.width*(320/self.imageView.image.size.width), self.imageView.image.size.height*(320/self.imageView.image.size.width)), NO, 1.0);
        [self.imageView.image drawInRect:CGRectMake(0,0, self.imageView.image.size.width*(320/self.imageView.image.size.width), self.imageView.image.size.height*(320/self.imageView.image.size.width))];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImageJPEGRepresentation(newImage, 1.0f);
        PFFile *imageFile = [PFFile fileWithName:@"picture.jpg" data:imageData];
        [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [PFCloud callFunctionInBackground:@"sendMessage" withParameters:
             @{@"sender":[RDRUser currentUser].phoneNumber ? : @"1234",
               @"receiver":_remoteUser.phoneNumber,
               @"senderName":[RDRUser currentUser].fullName ? : @"Test Sender",
               @"imageUrl":imageFile.url, @"receiverType":[NSNumber numberWithInt:_remoteUser.userType]}
                                        block:^(id object, NSError *error) {
                                            if(!error){
                                                NSLog(@"%@", object);
                                            }else{
                                                NSLog(@"sendMessage failed");
                                            }
                                        }];
        } progressBlock:^(int percentDone) {
            NSLog(@"Uploading Picture: %d%%", percentDone);
            [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveLinear animations:^{
                CGRect frame = self.progressView.frame;
                frame.size.width = self.view.bounds.size.width * percentDone/100.f;
                self.progressView.frame = frame;
            } completion:^(BOOL finished) {
            }];
        }];
    }
    [self.delegate sendChatViewControllerDidSendChat:self];
}

- (IBAction)onCameraButtonTapped:(id)sender
{
    void (^showImagePickerBlock)() = ^{
        UIImagePickerController *picker = [UIImagePickerController new];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        picker.allowsEditing = NO;
        [self.parentViewController presentViewController:picker animated:YES completion:nil];
    };
    
    void (^showCameraBlock)() = ^{
        UIImagePickerController *mediaPicker = [[UIImagePickerController alloc] init];
        [mediaPicker setDelegate:self];
        mediaPicker.allowsEditing = NO;
        mediaPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:mediaPicker animated:YES completion:nil];
    };
    
    void (^onCancelButtonBlock)() = ^{
        [self.messageTextView becomeFirstResponder];
        self.messageTextView.hidden = NO;
        self.sendMessageLabel.hidden = NO;
    };
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil cancelButtonItem:[RIButtonItem itemWithLabel:@"Cancel" action:^{onCancelButtonBlock();}] destructiveButtonItem:nil otherButtonItems:[RIButtonItem itemWithLabel:@"Take Picture" action:^{
            showCameraBlock();
        }], [RIButtonItem itemWithLabel:@"Choose Existing" action:^{
            showImagePickerBlock();
        }], nil];
        
        [actionSheet showInView:self.view];
        
    } else {
        showImagePickerBlock();
    }
    
    
    [self.messageTextView resignFirstResponder];
    [self.messageTextView setText:nil];
    self.messageTextView.hidden = YES;
    self.sendMessageLabel.hidden = YES;
}

- (IBAction)onBottomCameraButtonTapped:(id)sender
{
    void (^showImagePickerBlock)() = ^{
        UIImagePickerController *picker = [UIImagePickerController new];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        picker.allowsEditing = NO;
        [self.parentViewController presentViewController:picker animated:YES completion:nil];
    };
    
    void (^showCameraBlock)() = ^{
        UIImagePickerController *mediaPicker = [[UIImagePickerController alloc] init];
        [mediaPicker setDelegate:self];
        mediaPicker.allowsEditing = NO;
        mediaPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:mediaPicker animated:YES completion:nil];
    };
    
    void (^removePictureBlock)() = ^{
        self.imageView.image = nil;
        self.messageTextView.hidden = NO;
        self.sendMessageLabel.hidden = NO;
        [self enableNextButton:NO];
        self.cameraButton2.hidden = YES;
        self.cameraButton.hidden = !_enableCamera;
        self.reportButton.hidden = NO;
        [self.messageTextView becomeFirstResponder];
    };
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil cancelButtonItem:[RIButtonItem itemWithLabel:@"Cancel"] destructiveButtonItem:[RIButtonItem itemWithLabel:@"Remove Picture" action:^{
            removePictureBlock();
        }] otherButtonItems:[RIButtonItem itemWithLabel:@"Take Picture" action:^{
            showCameraBlock();
        }], [RIButtonItem itemWithLabel:@"Choose Existing" action:^{
            showImagePickerBlock();
        }], nil];
        
        [actionSheet showInView:self.view];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil cancelButtonItem:[RIButtonItem itemWithLabel:@"Cancel"] destructiveButtonItem:[RIButtonItem itemWithLabel:@"Remove Picture" action:^{
            removePictureBlock();
        }] otherButtonItems:[RIButtonItem itemWithLabel:@"Choose Existing" action:^{
            showImagePickerBlock();
        }], nil];
        [actionSheet showInView:self.view];
    }
}

#pragma mark - UIImagePickerControllerDelegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    self.imageView.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
    [self enableNextButton:YES];
    self.cameraButton.hidden = self.reportButton.hidden = YES;
    self.cameraButton2.hidden = NO;
    [self.imageView rdr_applyShadow];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    self.messageTextView.hidden = NO;
    self.sendMessageLabel.hidden = NO;
    [self.messageTextView becomeFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private
- (void)enableNextButton:(BOOL)enable
{
    if (enable) {
        [self.gradientView startAnimating];
        self.nextButton.enabled = YES;
    } else {
        [self.gradientView stopAnimating];
        self.nextButton.enabled = NO;
    }
}

#pragma mark - Message Composer Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            [PFCloud callFunctionInBackground:@"reportUser" withParameters:@{@"reporter":[RDRUser currentUser].phoneNumber,@"reportee":_remoteUser.phoneNumber} block:nil];
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.messageTextView becomeFirstResponder];
}

@end
