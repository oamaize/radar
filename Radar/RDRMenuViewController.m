//
//  RDRMenuViewController.m
//  Radar
//
//  Created by Rollin Su on 3/23/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRMenuViewController.h"
#import "RDRMainHeaderView.h"
#import "UIView+RDRRoundedCorners.h"
#import "UIView+RDRTheme.h"
#import "RDRAddressBookController.h"
#import "UIView+RDRUIImage.h"
#import "UIImage+RDRBlur.h"
#import "RDRUser.h"
#import "RDRMainPageViewController.h"
#import "RDRAddressBookController.h"
#import "RDRMCManager.h"

#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/AFNetworking.h>
#import <Parse/Parse.h>

@interface RDRMenuViewController () <UITextFieldDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UISwitch *dndSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *dummyDataSwitch;
@property (weak, nonatomic) IBOutlet UIView *sliderView;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *syncButton;
@property (weak, nonatomic) IBOutlet UIButton *disconnectButton;
@property (weak, nonatomic) IBOutlet UIButton *replaceImageButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *editNameIcon;
@property (weak, nonatomic) CALayer *blurMaskLayer;
@property (assign, nonatomic) BOOL infoUpdated;
@property (assign, nonatomic) BOOL imageChanged;
@property (assign, nonatomic) BOOL requireReload;
@property (copy, nonatomic) void (^onDimissHandler) (BOOL requireReload);
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *disconnectActivityIndicator;
@property (nonatomic, strong) NSString *commaSeparatedPhoneNumbers;


@end

@implementation RDRMenuViewController

- (instancetype)init
{
    self = [super initWithNibName:@"RDRMenuViewController" bundle:nil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    RDRMainHeaderView *header = [[RDRMainHeaderView alloc] initWithFrame:self.headerView.bounds type:RDRMainHeaderViewTypeSettings];
    [self.headerView addSubview:header];
    
    [header.closeButton addTarget:self action:@selector(onCloseButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.imageView setImageWithURL:[NSURL URLWithString:[RDRUser currentUser].imageURLString] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self roundThemViews];
    
    self.nameField.text = [NSString stringWithFormat:@"%@ %@", [RDRUser currentUser].firstName, [RDRUser currentUser].lastName];
    
    [self.syncButton rdr_applyInactiveButtonTheme];
    [self.disconnectButton rdr_applyInactiveButtonTheme];
    
    self.dummyDataSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyUseDummyData];
    self.dndSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyDoNotDisturb];
    
    CALayer *maskLayer = [CALayer layer];
    maskLayer.anchorPoint = CGPointMake(0.5f, 0.f);
    maskLayer.frame = CGRectMake(0, 0, 320.f, 0.f);
    maskLayer.backgroundColor = [UIColor whiteColor].CGColor;
    self.blurMaskLayer = maskLayer;
    self.backgroundImageView.layer.mask = maskLayer;
    
    
    NSDictionary *attributes = @{NSFontAttributeName: self.nameField.font};
    CGRect rect = [self.nameField.text boundingRectWithSize:CGSizeMake(self.nameField.bounds.size.width, MAXFLOAT)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:attributes
                                               context:nil];
    rect.origin.x = 5.f + self.nameField.frame.origin.x + (self.nameField.frame.size.width - rect.size.width) / 2.f;
    
    rect.origin.x = MAX(self.nameField.frame.origin.x + 0.6 * self.nameField.frame.size.width, rect.origin.x);
    CGRect iconFrame = self.editNameIcon.frame;
    iconFrame.origin.x = CGRectGetMaxX(rect);
    self.editNameIcon.frame = iconFrame;
    
    self.commaSeparatedPhoneNumbers = [[RDRAddressBookController sharedAddressBookController] friendsListNumbers];
}

-(void)roundThemViews
{
    self.imageView.layer.cornerRadius =
    self.shadowView.layer.cornerRadius = self.imageView.bounds.size.width / 2.f;
    self.imageView.clipsToBounds = YES;
    
    [self.shadowView rdr_applyShadow];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGRect __block frame = self.sliderView.frame;
    frame.origin.y = -CGRectGetHeight(self.sliderView.frame);
    self.sliderView.frame = frame;
    
    CGRect maskBounds = self.blurMaskLayer.bounds;
    maskBounds.size.height = self.backgroundImageView.bounds.size.height;
    
    CGFloat duration = .5f;
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"bounds"];
    [anim setFromValue:[NSValue valueWithCGRect:self.blurMaskLayer.bounds]];
    [anim setToValue:[NSValue valueWithCGRect:maskBounds]];
    [anim setDuration:duration];
    [anim setTimingFunction:[CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut]];
    [anim setFillMode:kCAFillModeForwards];
    
    self.blurMaskLayer.bounds = maskBounds;
    [self.blurMaskLayer addAnimation:anim forKey:@"mask"];
    
    [UIView animateWithDuration:duration animations:^{
        frame.origin.y = 0.f;
        self.sliderView.frame = frame;
        
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)onReplaceImageButtonTapped:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Picture",@"Choose Existing", nil];
        
        UIViewController *parentVC = self;
        while (parentVC.parentViewController) {
            parentVC = parentVC.parentViewController;
        }
        
        [actionSheet showInView:parentVC.view];
        
    } else {
        UIImagePickerController *picker = [UIImagePickerController new];
        picker.delegate = self;
        picker.allowsEditing = YES;
        
        [self.parentViewController presentViewController:picker animated:YES completion:nil];
    }
}

- (IBAction)onSyncAddressBookButtonTapped:(id)sender
{
    if (self.activityIndicator.isAnimating) {
        return;
    }
    
    // No Need to set requireReload, using Notification from addressbookcontroller
    //self.requireReload = YES;
    [self.activityIndicator startAnimating];
    [[RDRAddressBookController sharedAddressBookController] syncAddressBookOnComplete:^{
        [self.activityIndicator stopAnimating];
    }];
}

- (IBAction)onDoNotDisturbSwitchValueChanged:(UISwitch *)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:RDRDefaultsKeyDoNotDisturb];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [PFCloud callFunctionInBackground:@"doNotDisturb" withParameters:@{@"phoneNumber":[RDRUser currentUser].phoneNumber} block:^(id object, NSError *error) {
    }];
    if(sender.isOn){
        [[RDRMCManager sharedRDRMCManager] startBrowsing:false];
        [[RDRMCManager sharedRDRMCManager] advertiseSelf:false];
    }else{
        [[RDRMCManager sharedRDRMCManager] startBrowsing:true];
        [[RDRMCManager sharedRDRMCManager] advertiseSelf:true];
    }
}

- (IBAction)onUseDummyDataSwitchValueChanged:(UISwitch *)sender
{
    self.requireReload = YES;
    [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:RDRDefaultsKeyUseDummyData];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)onDisconnectDeviceButtonTapped:(id)sender
{
    [self.disconnectActivityIndicator startAnimating];
    self.view.userInteractionEnabled = NO;
    [PFCloud callFunctionInBackground:@"doNotDisturb" withParameters:@{@"phoneNumber":[RDRUser currentUser].phoneNumber} block:^(id object, NSError *error) {
        if (error) {
            NSLog(@"Disconnect error: %@", error);
        }
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:RDRDefaultsKeyIsDisconnected];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[RDRMCManager sharedRDRMCManager] startBrowsing:false];
        [[RDRMCManager sharedRDRMCManager] advertiseSelf:false];
        
        [self onCloseButtonTapped:self];
        [self.disconnectActivityIndicator stopAnimating];
        self.view.userInteractionEnabled = YES;
    }];
}

- (void)onCloseButtonTapped:(id)sender
{
    
    CGRect maskBounds = self.blurMaskLayer.bounds;
    maskBounds.size.height = 0.f;
    
    CGFloat duration = .5f;
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"bounds"];
    [anim setFromValue:[NSValue valueWithCGRect:self.blurMaskLayer.bounds]];
    [anim setToValue:[NSValue valueWithCGRect:maskBounds]];
    [anim setDuration:duration];
    [anim setTimingFunction:[CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut]];
    [anim setFillMode:kCAFillModeForwards];
    
    self.blurMaskLayer.bounds = maskBounds;
    [self.blurMaskLayer addAnimation:anim forKey:@"mask"];
    
    [self updateUserInfo];
    
    
    [UIView animateWithDuration:duration animations:^{
        CGRect frame = self.sliderView.frame;
        frame.origin.y = -CGRectGetHeight(self.sliderView.frame);
        self.sliderView.frame = frame;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        if (self.onDimissHandler) {
            self.onDimissHandler(self.requireReload);
        }
    }];
}

- (void) updateUserInfo
{
    if (!self.infoUpdated && !self.imageChanged) {
        return;
    }
    NSString *trimmed = [self.nameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSArray* nameArray = [trimmed componentsSeparatedByString: @" "];
    
    RDRUser *currentUser = [RDRUser currentUser];
    
    if (nameArray.count > 0) {
        currentUser.firstName = nameArray[0];
    }
    if (nameArray.count > 1) {
        currentUser.lastName = nameArray[1];
    }
    
    // Force a disk save
    [RDRUser setCurrentUser:currentUser];
    
    
    // TODO: change this code to single step upload + association, instead of upload, then associate
    void (^serverCallBlock)(PFFile *imageFile) = ^ (PFFile *imageFile) {
        [PFCloud callFunctionInBackground:@"updateUserInfoAndNotifyFriends" withParameters:@{@"phoneNumber":currentUser.phoneNumber, @"firstName":currentUser.firstName, @"lastName": currentUser.lastName, @"friendsArray":self.commaSeparatedPhoneNumbers} block:^(id object, NSError *error) {
            if (imageFile) {
                object[@"picture"] = imageFile;
            }
            
            
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    
                    currentUser.imageURLString = ((PFFile *)object[@"picture"]).url;
                    [self.imageView setImageWithURL:[NSURL URLWithString:currentUser.imageURLString] placeholderImage:[UIImage imageNamed:@"placeholder"]];
                    [RDRUser setCurrentUser:currentUser];
                    
                    NSLog(@"completed user info update..");
                }
            }];
        }];
    };
    
    if (self.imageChanged) {
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.imageView.image.size.width*0.4, self.imageView.image.size.height*0.4), NO, 1.0);
        [self.imageView.image drawInRect:CGRectMake(0,0, self.imageView.image.size.width*0.4, self.imageView.image.size.height*0.4)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImagePNGRepresentation(newImage);
        PFFile *imageFile = [PFFile fileWithName:@"ownpic.png" data:imageData];
        [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(succeeded){
                serverCallBlock(imageFile);
            }
        }];
    } else {
        serverCallBlock(nil);
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSArray *subviews = [self.headerView subviews];
    ((RDRMainHeaderView *)subviews[0]).closeButton.enabled = NO;
    self.replaceImageButton.enabled = NO;
    self.editNameIcon.hidden = YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *trimmed = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSArray* nameArray = [trimmed componentsSeparatedByString: @" "];
    if([nameArray count] != 2){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Please enter your first and last name (first last)." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
        return NO;
    } else {
        if(![nameArray[0] isEqualToString:[RDRUser currentUser].firstName] || ![nameArray[1] isEqualToString:[RDRUser currentUser].lastName]){
            self.infoUpdated = YES;
        }
        NSArray *subviews = [self.headerView subviews];
        ((RDRMainHeaderView *)subviews[0]).closeButton.enabled = YES;
        self.replaceImageButton.enabled = YES;
        [textField resignFirstResponder];
        
        NSDictionary *attributes = @{NSFontAttributeName: textField.font};
        CGRect rect = [textField.text boundingRectWithSize:CGSizeMake(textField.bounds.size.width, MAXFLOAT)
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                attributes:attributes
                                                   context:nil];
        CGRect rect2 = [textField.placeholder boundingRectWithSize:CGSizeMake(textField.bounds.size.width, MAXFLOAT)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:attributes
                                                           context:nil];
        
        CGRect theRect = CGRectGetMaxX(rect) > CGRectGetMaxX(rect2) ? rect : rect2;
        theRect.origin.x = 5.f + textField.frame.origin.x + (textField.frame.size.width - rect.size.width) / 2.f;
        
        CGRect iconFrame = self.editNameIcon.frame;
        iconFrame.origin.x = CGRectGetMaxX(theRect);
        self.editNameIcon.frame = iconFrame;
        self.editNameIcon.hidden = NO;
        return YES;
    }
    
}

#pragma mark - UIImagePickerControllerDelegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    self.imageView.image = image;
    [RDRUser currentUser].image = image;
    self.imageChanged = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate Methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0) {
        UIImagePickerController *mediaPicker = [[UIImagePickerController alloc] init];
        [mediaPicker setDelegate:self];
        mediaPicker.allowsEditing = YES;
        mediaPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        mediaPicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        [self presentViewController:mediaPicker animated:YES completion:nil];
        
    } else if(buttonIndex == 1) {
        UIImagePickerController *mediaPicker = [[UIImagePickerController alloc] init];
        [mediaPicker setDelegate:self];
        mediaPicker.allowsEditing = YES;
        mediaPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:mediaPicker animated:YES completion:nil];
    }
}

@end



#pragma mark - Category on UIViewController to present this shit

@implementation UIViewController (RDRMenuViewController)

- (void)rdr_presentSettingsViewControllerOnDismiss:(void (^)(BOOL))onDismiss
{
    //Blur image
    UIImage *screenCapImage = [self.view rdr_image];
    UIImage *blurredImage = [screenCapImage rdr_blurredImageWithRadius:0.3f];
    
    RDRMenuViewController *menu = [RDRMenuViewController new];
    menu.onDimissHandler = onDismiss;
    
    [self addChildViewController:menu];
    [self.view addSubview:menu.view];
    
    menu.backgroundImageView.image = blurredImage;
}

@end
