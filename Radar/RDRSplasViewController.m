//
//  RDRSplasViewController.m
//  Radar
//
//  Created by Rollin Su on 2/25/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRSplasViewController.h"

@interface RDRSplasViewController ()

@end

@implementation RDRSplasViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
