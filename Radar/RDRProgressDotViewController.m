//
//  RDRProgressDotViewController.m
//  Radar
//
//  Created by Rollin Su on 2/24/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRProgressDotViewController.h"

@interface RDRProgressDotViewController ()
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *dots;

@end

@implementation RDRProgressDotViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    for (UIImageView *iv in self.dots) {
        iv.image = [UIImage imageNamed:@"progress_dot_not_current"];
    }
    
    if (self.dots.count > 0) {
        self.currentIndex = 0;
    } else {
        _currentIndex = 0;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods
- (void)setCurrentIndex:(NSInteger)currentIndex
{
    if (currentIndex < 0 || currentIndex >= self.dots.count) {
        @throw ([NSException exceptionWithName:NSRangeException reason:@"index out of bound" userInfo:nil]);
    }
    [(UIImageView*)self.dots[_currentIndex] setImage:[UIImage imageNamed:@"progress_dot_not_current"]];
    [(UIImageView*)self.dots[currentIndex] setImage:[UIImage imageNamed:@"progress_dot_current"]];
    
    _currentIndex = currentIndex;
}

@end
