//
//  RDRConnectDeviceViewController.m
//  Radar
//
//  Created by Rollin Su on 2/24/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRConnectDeviceViewController.h"
#import "UIView+RDRTheme.h"
#import "RDRUser.h"
#import "RDRMCManager.h"

#import <QuartzCore/QuartzCore.h>
#import <Parse/Parse.h>

@interface RDRConnectDeviceViewController ()
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation RDRConnectDeviceViewController

- (instancetype)init
{
    self = [super initWithNibName:@"RDRConnectDeviceViewController" bundle:nil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.connectButton rdr_applyButtonTheme];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction
- (IBAction)onConnectButtonTapped:(id)sender {
    
    [sender setEnabled:NO];
    void (^dismissBlock)(void) = ^{
        [UIView animateWithDuration:0.3f animations:^{
            self.view.alpha = 0.f;
        } completion:^(BOOL finished) {
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:RDRDefaultsKeyIsDisconnected];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }];
    };
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:RDRDefaultsKeyIsDisconnected]) {
        [self.spinner startAnimating];
        
        [PFCloud callFunctionInBackground:@"doNotDisturb" withParameters:@{@"phoneNumber":[RDRUser currentUser].phoneNumber} block:^(id object, NSError *error) {
            if (error) {
                NSLog(@"doNotDisturb call failed: %@", error);
            }
            [[RDRMCManager sharedRDRMCManager] startBrowsing:true];
            [[RDRMCManager sharedRDRMCManager] advertiseSelf:true];
            dismissBlock();
        }];
    } else {
        // first time flow
        dismissBlock();
    }
}
@end
