//
//  RDRFakeMessasgeSender.h
//  Radar
//
//  Created by Rollin Su on 3/5/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RDRUser;
@interface RDRFakeMessasgeSender : NSObject

- (void)startFakeMessagesFromUser:(RDRUser *)user timeInterval:(NSTimeInterval)interval;
- (void)stopFakeMessagesFromUser:(RDRUser *)user;
- (void)stopAllMessages;

@end
