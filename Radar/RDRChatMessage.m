//
//  RDRChatMessage.m
//  Radar
//
//  Created by Rollin Su on 12/4/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRChatMessage.h"

@implementation RDRChatMessage


- (id)initWithMessage:(NSString*)message sender:(id)sender timestamp:(NSDate*)timestamp
{
    self = [super init];
    if (self) {
        _message = [[NSString alloc] initWithString:message];
        _sender = sender;
        _timestamp = timestamp;
        _unread = YES;
    }
    return self;
}
@end
