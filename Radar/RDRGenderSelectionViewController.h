//
//  RDRGenderSelectionViewController.h
//  Radar
//
//  Created by Rollin Su on 11/20/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum RDRUserGender : NSInteger RDRUserGender;
@protocol RDRGenderSelectionViewControllerDelegate;
@interface RDRGenderSelectionViewController : UIViewController

@property (nonatomic, weak) id<RDRGenderSelectionViewControllerDelegate> delegate;
@property (nonatomic, weak) IBOutlet UISegmentedControl *genderSelection;

@end

@protocol RDRGenderSelectionViewControllerDelegate <NSObject>

- (void)genderController:(RDRGenderSelectionViewController*)controller didSelectUserGender:(RDRUserGender)gender;

@end
