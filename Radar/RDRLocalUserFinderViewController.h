//
//  RDRLocalUserFinderViewController.h
//  Radar
//
//  Created by Rollin Su on 3/19/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CLLocationManager;
@interface RDRLocalUserFinderViewController : UIViewController

- (instancetype)initWithLocationManager:(CLLocationManager *)locationManager;

@end
