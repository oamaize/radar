//
//  RDRSmsVerifyViewController.h
//  Radar
//
//  Created by Ota-Benga Amaize on 11/16/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


@protocol RDRSmsVerifyViewControllerDelegate;
@interface RDRSmsVerifyViewController : UIViewController<UITextFieldDelegate>
@property (nonatomic, weak) id<RDRSmsVerifyViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;

- (void)reset;
@end


@protocol RDRSmsVerifyViewControllerDelegate<NSObject>
- (void)smsVerifyViewController:(RDRSmsVerifyViewController*)vc didSubmitVerificationWithFirstName:(NSString*)firstName lastName:(NSString*)lastName;
@end
