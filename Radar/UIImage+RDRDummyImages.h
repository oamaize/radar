//
//  UIImage+RDRDummyImages.h
//  Radar
//
//  Created by Rollin Su on 12/8/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (RDRDummyImages)

+ (UIImage*)rdr_roundedImageNamed:(NSString*)filename;
@end
