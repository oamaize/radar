//
//  RDRPopUpViewController.h
//  Radar
//
//  Created by Rollin Su on 1/23/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDRSendChatViewController.h"

@interface RDRPopUpViewController : UIViewController

@end


@interface UIViewController (RDRPopUpViewController)
- (void)rdr_popUpViewController:(UIViewController *)vc fromRect:(CGRect)rect enablePanDismiss:(BOOL)enablePanDismiss onComplete:(void (^)())onCompleteHandler onDismiss:(void (^)())onDismissHandler;
- (void)rdr_dismissPopUpViewController;
@end