//
//  RDRMainThumbnailCell.m
//  Radar
//
//  Created by Rollin Su on 11/15/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRMainThumbnailCell.h"
#import "UIView+RDRTheme.h"
#import <JSBadgeView/JSBadgeView.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>

@interface RDRMainThumbnailCell()
@property (nonatomic, weak) CAShapeLayer *borderLayer;
@end

@implementation RDRMainThumbnailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)prepareForReuse
{
    @synchronized(self) {
    [super prepareForReuse];
    [self.imageView cancelImageRequestOperation];
    self.imageView.image = nil;
    }
}

- (void)setBorder:(RDRMainThumbnailCellBorder)border
{
    _border = border;
    if (!self.borderLayer) {
        CAShapeLayer *layer = [[CAShapeLayer alloc] init];
        [self.layer addSublayer:layer];
        self.borderLayer = layer;
    }
    [self reloadBorders];
}

- (void)setup
{
    JSBadgeView *badge = [[JSBadgeView alloc] initWithParentView:self alignment:JSBadgeViewAlignmentTopRight];
    badge.badgeText = nil;
    badge.badgePositionAdjustment = CGPointMake(-16, 12);
    _badgeView = badge;
    _nameLabel.layer.shadowOpacity = 1.0;
    _nameLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    _nameLabel.layer.shadowOffset = CGSizeMake(0.0,-1.0);
    _nameLabel.layer.shadowRadius = 0.0;
}

-(void) reloadBorders
{
    if (!self.borderLayer) {
        return;
    }
    
    CGFloat selectiveBordersWidth = 2.f;
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    if (_border & RDRMainThumbnailCellBorderLeft) { // left border
        CGPoint startPoint = CGPointMake(0-selectiveBordersWidth/2, 0);
        CGPoint endPoint = CGPointMake(0-selectiveBordersWidth/2, CGRectGetMaxY(self.bounds));
        [path moveToPoint:startPoint];
        [path addLineToPoint:endPoint];
    }
    if (_border & RDRMainThumbnailCellBorderRight) { // right border
        CGPoint startPoint = CGPointMake(CGRectGetMaxX(self.bounds)-selectiveBordersWidth/2, 0);
        CGPoint endPoint = CGPointMake(CGRectGetMaxX(self.bounds)-selectiveBordersWidth/2, CGRectGetMaxY(self.bounds));
        [path moveToPoint:startPoint];
        [path addLineToPoint:endPoint];
    }
    if (_border & RDRMainThumbnailCellBorderTop) { // top border
        CGPoint startPoint = CGPointMake(0, 0+selectiveBordersWidth/2);
        CGPoint endPoint = CGPointMake(CGRectGetMaxX(self.bounds), 0+selectiveBordersWidth/2);
        [path moveToPoint:startPoint];
        [path addLineToPoint:endPoint];
    }
    if (_border & RDRMainThumbnailCellBorderBottom) { // bottom border
        CGPoint startPoint = CGPointMake(0, CGRectGetMaxY(self.bounds)-selectiveBordersWidth/2);
        CGPoint endPoint = CGPointMake(CGRectGetMaxX(self.bounds), CGRectGetMaxY(self.bounds)-selectiveBordersWidth/2);
        [path moveToPoint:startPoint];
        [path addLineToPoint:endPoint];
    }
    
    _borderLayer.frame = self.bounds;
    _borderLayer.path = path.CGPath;
    
    _borderLayer.strokeColor = [UIView rdr_primaryThemeColor].CGColor;
    _borderLayer.lineWidth = selectiveBordersWidth;
}

@end
