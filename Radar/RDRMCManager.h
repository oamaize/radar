//
//  RDRMCManager.h
//  Radar
//
//  Created by Ota-Benga Amaize on 4/26/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@interface RDRMCManager : NSObject <MCNearbyServiceAdvertiserDelegate, MCNearbyServiceBrowserDelegate>

+ (instancetype)sharedRDRMCManager;

@property (nonatomic, strong) MCPeerID *peerID;
//@property (nonatomic, strong) MCSession *session;
@property (nonatomic, strong) MCNearbyServiceAdvertiser *advertiser;
@property (nonatomic, strong) MCNearbyServiceBrowser *browser;

#pragma mark - Public Methods
-(void)setupWithDisplayName:(NSString *)displayName;
-(void)advertiseSelf:(BOOL)shouldAdvertise;
-(void)startBrowsing:(BOOL)shouldBrowse;

@end
