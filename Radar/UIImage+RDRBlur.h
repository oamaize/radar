//
//  UIImage+RDRBlur.h
//  Radar
//
//  Created by Rollin Su on 1/22/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (RDRBlur)

- (UIImage*)rdr_blurredImageWithRadius:(CGFloat)radius;
@end
