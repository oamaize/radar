//
//  RDRSlideUpViewController.h
//  Radar
//
//  Created by Rollin Su on 1/16/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController(RDRSlideUpViewController)
- (void)rdr_slideupViewController:(UIViewController*)vc;
@end

@interface RDRSlideUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;
@end
