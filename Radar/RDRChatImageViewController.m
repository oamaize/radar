//
//  RDRChatImageViewController.m
//  Radar
//
//  Created by Rollin Su on 3/9/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRChatImageViewController.h"
#import "RDRUser.h"
#import "UIView+RDRTheme.h"
#import "RDRPopUpViewController.h"
#import "UIImage+RDRRotation.h"

#import <AFNetworking/AFNetworking.h>

@interface RDRChatImageViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (copy, nonatomic) NSString *imageUrl;
@end

@implementation RDRChatImageViewController

- (instancetype) initWithSender:(RDRUser *)user imageURL:(NSString *)imageUrl
{
    self = [super initWithNibName:@"RDRChatImageViewController" bundle:nil];
    if (self) {
        self.imageUrl = imageUrl;
        self.view.backgroundColor = [UIColor clearColor]; //force a view did load
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    
    CGRect frame = self.progressView.frame;
    frame.size.width = 0.f;
    self.progressView.frame = frame;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_imageUrl]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView.image = image;
            
            CGRect frame = self.progressView.frame;
            frame.size.width = 0.f;
            self.progressView.frame = frame;
            
            [UIView animateWithDuration:8.f animations:^{
                CGRect f = self.progressView.frame;
                f.size.width = self.view.bounds.size.width;
                self.progressView.frame = f;
            } completion:^(BOOL finished) {
                if (finished) {
                    [self rdr_dismissPopUpViewController];
                }
            }];

        });
    });
}

-(void)appWillResignActive:(NSNotification*)notif
{
    [self rdr_dismissPopUpViewController];
}
-(void)appWillTerminate:(NSNotification*)notif
{
    [self rdr_dismissPopUpViewController];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
