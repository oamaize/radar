//
//  RDRMenuViewController.h
//  Radar
//
//  Created by Rollin Su on 3/23/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDRMenuViewController : UIViewController

@end



#pragma mark - Category
@interface UIViewController (RDRMenuViewController)

- (void)rdr_presentSettingsViewControllerOnDismiss:(void (^)(BOOL requireReload))onDismiss;
@end