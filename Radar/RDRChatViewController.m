//
//  RDRChatViewController.m
//  Radar
//
//  Created by Rollin Su on 3/9/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRChatViewController.h"
#import "RDRUser.h"
#import "UIView+RDRTheme.h"
#import "RDRPopUpViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/AFNetworking.h>

@interface RDRChatViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *progressView;

@end

@implementation RDRChatViewController

- (instancetype)initWithSender:(RDRUser *)sender message:(NSString *)message
{
    return [self initWithSender:sender message:message image:nil];
}

- (instancetype)initWithSender:(RDRUser *)sender image:(UIImage *)image
{
    return [self initWithSender:sender message:nil image:image];
}

- (instancetype)initWithSender:(RDRUser *)sender message:(NSString *)message image:(UIImage *)image
{
    self = [super initWithNibName:@"RDRChatViewController" bundle:nil];
    if (self) {
        self.view.backgroundColor = [UIColor clearColor]; //force view did load
        //_imageView.image = sender.image;
        [_imageView setImageWithURL:[NSURL URLWithString:sender.imageURLString]];
        _nameLabel.text = sender.fullName;
        if (message) {
            _messageTextView.text = message;
            _messageTextView.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:22];
            _messageTextView.textAlignment = NSTextAlignmentCenter;
        }
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    
    CGRect frame = self.progressView.frame;
    frame.size.width = 0.f;
    self.progressView.frame = frame;
    
    [UIView animateWithDuration:8.f animations:^{
        CGRect f = self.progressView.frame;
        f.size.width = self.view.bounds.size.width;
        self.progressView.frame = f;
    } completion:^(BOOL finished) {
        if (finished) {
            [self rdr_dismissPopUpViewController];
        }
    }];
    
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self roundThemViews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)roundThemViews
{
    self.imageView.layer.cornerRadius =
    self.shadowView.layer.cornerRadius = self.imageView.bounds.size.width / 2.f;
    self.imageView.clipsToBounds = YES;
    
    
    [self.shadowView rdr_applyShadow];
}

-(void)appWillResignActive:(NSNotification*)notif
{
    [self rdr_dismissPopUpViewController];
}
-(void)appWillTerminate:(NSNotification*)notif
{
    [self rdr_dismissPopUpViewController];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
}

@end
