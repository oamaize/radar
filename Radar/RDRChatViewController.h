//
//  RDRChatViewController.h
//  Radar
//
//  Created by Rollin Su on 3/9/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RDRUser;
@interface RDRChatViewController : UIViewController

- (instancetype)initWithSender:(RDRUser *)sender message:(NSString *)message;
- (instancetype)initWithSender:(RDRUser *)sender image:(UIImage *)image;

@end
