//
//  RadarAppDelegate.m
//  Radar
//
//  Created by Ota-Benga Amaize on 11/1/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRRadarAppDelegate.h"
#import "RDRSmsVerifyViewController.h"
#import "RDRMainViewController.h"
#import "UIColor+RDRHex.h"
#import "TestFlight.h"
#import "RDRMasterPageViewController.h"
#import <Parse/Parse.h>

NSString * const kImageKey = @"kImageKey";

@implementation RDRRadarAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    // Uncomment bottom code to switch back to non-storyboard initialization. Also need to remove Main Storyboard file... entry from info plist
    
    /*
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    RDRMasterPageViewController *masterPageViewController = [RDRMasterPageViewController new];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:masterPageViewController];
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
     */
    
    
    //[TestFlight takeOff:@"2b802d5e-dce2-4386-b961-9370582a2edd"];
    
    /* staging parse server keys*/
    [Parse setApplicationId:@"tCTtrB7AedqQ06AKFNEVjf87dRVGW3OPX3ORnWW5"
                  clientKey:@"DWAY1EGkiLYSXag9il7egr93QevX8HqXj7UVtwWV"];
    
    
    /* production parse server keys
    [Parse setApplicationId:@"HpiqvdTC4Dxeog9eFqN1qPwffEEvHF64P571o3Gz"
                  clientKey:@"xFluBtTwidxVfmPIcCxyTztKbm7ED1hgamzUrCNJ"];
    */
    
    
    //[PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    if (application.applicationState != UIApplicationStateBackground) {
        // Track an app open here if we launch with a push, unless
        // "content_available" was used to trigger a background push (introduced
        // in iOS 7). In that case, we skip tracking here to avoid double counting
        // the app-open.
        BOOL preBackgroundPush = ![application respondsToSelector:@selector(backgroundRefreshStatus)];
        BOOL oldPushHandlerOnly = ![self respondsToSelector:@selector(application:didReceiveRemoteNotification:fetchCompletionHandler:)];
        BOOL noPushPayload = ![launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (preBackgroundPush || oldPushHandlerOnly || noPushPayload) {
            [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
        }
    }
    
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|
     UIRemoteNotificationTypeAlert|
     UIRemoteNotificationTypeSound]; //push notification
    
    NSLog(@"application did finish launching...");
    // Override point for customization after application launch.
    
    // Customize the title text for *all* UINavigationBars
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor rdr_colorWithHexLiteral:0xFFFFFF],
      NSForegroundColorAttributeName,
      [UIFont fontWithName:@"GillSans" size:32.0],
      NSFontAttributeName,
      nil]];
    
    return YES;
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    return YES;
}


- (void)registerViewController:(NSString *)name controller:(UIViewController *)controller
{
    if(_viewControllers == nil)
    {
        _viewControllers = [[NSMutableDictionary alloc] init];
        
    }
    
    [_viewControllers setObject:controller forKey:name];
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Failed to register for remote notifications..");
}

//** this method below is for ios 7 background push
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if (userInfo) {
        if ([userInfo[@"type"] isEqualToString:@"verification"]) {
            NSLog(@"Verification Push Received.");
            [[NSNotificationCenter defaultCenter] postNotificationName:RDRNotificationDeviceVerified object:nil userInfo:userInfo];
        } else if([userInfo[@"type"] isEqualToString:@"message"]){
            NSLog(@"Message Push Received.");
            [UIApplication sharedApplication].applicationIconBadgeNumber++;
            [[NSNotificationCenter defaultCenter] postNotificationName:RDRNotificationIncomingMessage object:nil userInfo:userInfo];
        }
    }
}

//*******

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    currentInstallation.badge = [UIApplication sharedApplication].applicationIconBadgeNumber;
    [currentInstallation saveEventually];
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    currentInstallation.badge = [UIApplication sharedApplication].applicationIconBadgeNumber;
    [currentInstallation saveEventually];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if (currentInstallation.badge != 0){
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    currentInstallation.badge = [UIApplication sharedApplication].applicationIconBadgeNumber;
    [currentInstallation saveEventually];
}

@end
