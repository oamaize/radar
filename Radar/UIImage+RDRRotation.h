//
//  UIImage+RDRRotation.h
//  Radar
//
//  Created by Rollin Su on 3/16/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (RDRRotation)

-(UIImage*)rdr_imageWithscaleAndRotateImage;
@end
