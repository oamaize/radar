//
//  RDRAddressBookController.h
//  Radar
//
//  Created by Rollin Su on 3/23/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDRAddressBookController : NSObject

+ (instancetype)sharedAddressBookController;

- (void)syncAddressBookOnComplete:(void (^)(void))onComplete;
- (NSArray *)friendsList;
- (NSString *)friendsListNumbers;
- (void) reorderFriendsListByPuttingFriendsOnTop:(NSArray *)friends notify:(BOOL)notify;
@end
