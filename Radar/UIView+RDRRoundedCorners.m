//
//  UIView+RDRRoundedCorners.m
//  Radar
//
//  Created by Rollin Su on 3/23/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "UIView+RDRRoundedCorners.h"

@implementation UIView (RDRRoundedCorners)


- (void)rdr_roundCorners
{
    [self rdr_roundCornersWithRadius:MIN(CGRectGetHeight(self.bounds), CGRectGetWidth(self.bounds)) * 0.5f];
}

- (void)rdr_roundCornersWithRadius:(CGFloat)radius
{
    [self rdr_roundCorners:UIRectCornerAllCorners radius:radius];
}

- (void)rdr_roundCorners:(UIRectCorner)corners radius:(CGFloat)radius
{
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    circleLayer.frame = self.bounds;
    circleLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)].CGPath;
    circleLayer.fillColor = [UIColor whiteColor].CGColor;
    circleLayer.backgroundColor = [UIColor clearColor].CGColor;
    self.layer.mask = circleLayer;
}

@end
