//
//  RDRChatMessage.h
//  Radar
//
//  Created by Rollin Su on 12/4/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDRChatMessage : NSObject
@property (nonatomic, readonly) NSString *message;
@property (nonatomic, readonly) id sender; //class TBD
@property (nonatomic, readonly) NSDate *timestamp;
@property (nonatomic, assign, getter = isUnread) BOOL unread;

- (id)initWithMessage:(NSString*)message sender:(id)sender timestamp:(NSDate*)timestamp;
@end
