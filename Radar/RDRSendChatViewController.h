//
//  RDRSendChatViewController.h
//  Radar
//
//  Created by Ota-Benga Amaize on 2/20/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "RDRUser.h"

@protocol RDRSendChatViewControllerDelegate;
@interface RDRSendChatViewController : UIViewController <MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) id<RDRSendChatViewControllerDelegate> delegate;
@property (nonatomic, strong, readonly) RDRUser *remoteUser;

- (instancetype)initWithRemoteUser:(RDRUser *)user enableCamera:(BOOL)enableCamera;

@end

@protocol RDRSendChatViewControllerDelegate<NSObject>
- (void)sendChatViewControllerDidTapOnCloseButton:(RDRSendChatViewController *)vc;
- (void)sendChatViewControllerDidSendChat:(RDRSendChatViewController *)vc;
@end