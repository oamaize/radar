//
//  RDRConstants.h
//  Radar
//
//  Created by Rollin Su on 1/16/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#ifndef Radar_RDRConstants_h
#define Radar_RDRConstants_h


static NSString * const kRadarPin = @"kRadarPin";
static NSString * const RDRServiceType = @"rdr-service";

static NSString * const RDRDefaultsKeyUseDummyData = @"RDRDefaultsKeyUseDummyData";
static NSString * const RDRDefaultsKeyGenderFilter = @"RDRDefaultsKeyGenderFilter";
static NSString * const RDRDefaultsKeyDoNotDisturb = @"RDRDefaultsKeyDoNotDisturb";
static NSString * const RDRDefaultsKeySkipFindFriendsButton = @"RDRDefaultsKeySkipFindFriendsButton";
static NSString * const RDRDefaultsKeyIsDisconnected = @"RDRDefaultsKeyIsDisconnected";
static NSString * const RDRDefaultsKeyHintShowCount = @"RDRDefaultsKeyHintShowCount";

static const NSInteger RDRGenderAll = 0;
static const NSInteger RDRGenderMale = 1;
static const NSInteger RDRGenderFemale = 2;

static NSString * const RDRNotificationIncomingMessage = @"RDRNotificationIncomingMessage";
static NSString * const RDRNotificationDeviceVerified = @"RDRNotificationDeviceVerified";
static NSString * const RDRNotificationFriendsListUpdated = @"RDRNotificationFriendsListUpdated";


#endif
