//
//  UIImage+RDRDummyImages.m
//  Radar
//
//  Created by Rollin Su on 12/8/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "UIImage+RDRDummyImages.h"

@implementation UIImage (RDRDummyImages)

+ (UIImage*)rdr_roundedImageNamed:(NSString *)filename
{
    UIImage *image = [UIImage imageNamed:filename];
    CGFloat width = MIN(image.size.width, image.size.height);
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, width, width);
    imageLayer.contents = (id) image.CGImage;
    imageLayer.contentsGravity = kCAGravityCenter;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = width * 0.5f;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, width));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}
@end
