//
//  RDRGradientActivityIndicatorView.m
//  Radar
//
//  Created by Rollin Su on 2/4/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRGradientActivityIndicatorView.h"
#import <QuartzCore/QuartzCore.h>

@interface RDRGradientActivityIndicatorView()
@property (nonatomic, strong, readwrite) CALayer *gradientLayer;
@end

@implementation RDRGradientActivityIndicatorView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    //set gradient colors
    gradient.colors = @[(__bridge id)[UIColor blackColor].CGColor, (__bridge id)[UIColor darkGrayColor].CGColor, (__bridge id)[UIColor whiteColor].CGColor, (__bridge id)[UIColor darkGrayColor].CGColor, (__bridge id)[UIColor blackColor].CGColor];
    //set gradient start and end points
    gradient.locations = @[@0, @0, @0.2, @0.4, @1];
    gradient.startPoint = CGPointMake(0, 0.5);
    gradient.endPoint = CGPointMake(1, 0.5);
    gradient.opacity = 0.55f;
    [self.layer insertSublayer:gradient atIndex:0];
    self.gradientLayer = gradient;
    self.hidden = YES;
}

- (void)startAnimating
{
    if ([self.gradientLayer animationForKey:@"anim"] == nil) {
        //apply swinging animation
        CABasicAnimation *animation = [CABasicAnimation animation];
        animation.keyPath = @"locations";
        animation.duration = 1.4f;
        animation.repeatDuration = INFINITY;
        animation.autoreverses = self.isAutoReverse;
        animation.toValue =  @[@0, @0.6, @0.8, @1, @1];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        self.hidden = NO;
        [self.gradientLayer addAnimation:animation forKey:@"anim"];
    }
}

- (void)stopAnimating
{
    [self.gradientLayer removeAllAnimations];
    self.hidden = YES;
}

@end
