//
//  RDRMasterPageViewController.m
//  Radar
//
//  Created by Rollin Su on 11/25/13.
//  Copyright (c) 2013 Ota-Benga Amaize. All rights reserved.
//

#import "RDRMasterPageViewController.h"
#import "RDRMenuViewController.h"
#import "RDRMainViewController.h"
#import "RDRSignUpViewController.h"

@interface RDRMasterPageViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (nonatomic, strong) NSArray *viewControllers;
@property (nonatomic, strong) UIPageViewController *pageViewController;
@end

@implementation RDRMasterPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    self.pageViewController.view.frame = self.view.bounds;
    
    self.viewControllers = @[[RDRMainViewController new],
                             [RDRMainViewController new]];
    
    [self.pageViewController setViewControllers:@[self.viewControllers[1]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon_active"] style:UIBarButtonItemStylePlain target:self action:@selector(onMenuButtonTapped:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chat_icon_active"] style:UIBarButtonItemStylePlain target:self action:@selector(onChatButtonTapped:)];
    //self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    [self presentViewController:[RDRSignUpViewController new] animated:NO completion:nil];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private
- (void)setup
{
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    
}

- (void)onMenuButtonTapped:(id)sender
{
//    RDRMasterPageViewController * __weak weakSelf = self;
//    if (self.pageViewController.viewControllers[0] == self.viewControllers[2]) {
//        [self.pageViewController setViewControllers:@[self.viewControllers[1]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:^(BOOL finished) {
//            [weakSelf.pageViewController setViewControllers:@[weakSelf.viewControllers[0]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
//        }];
//    } else {
//        [self.pageViewController setViewControllers:@[self.viewControllers[0]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
//    }
}

- (void)onChatButtonTapped:(id)sender
{
//    RDRMasterPageViewController * __weak weakSelf = self;
//    if (self.pageViewController.viewControllers[0] == self.viewControllers[0]) {
//        [self.pageViewController setViewControllers:@[self.viewControllers[1]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
//            [weakSelf.pageViewController setViewControllers:@[weakSelf.viewControllers[2]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
//        }];
//    } else {
//        [self.pageViewController setViewControllers:@[self.viewControllers[2]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
//    }
}

#pragma mark - UIPageViewDataSource
- (UIViewController*)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger index = [self.viewControllers indexOfObject:viewController];
    if (index == 0) {
        return self.viewControllers[1];
    } else {
        return nil;
    }
}

- (UIViewController*)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger index = [self.viewControllers indexOfObject:viewController];
    if (index == 1) {
        return self.viewControllers[0];
    } else {
        return nil;
    }
}

#pragma mark - UIPageViewDelegate
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    
}

@end
