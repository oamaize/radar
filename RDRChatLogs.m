//
//  RDRChatLogs.m
//  Radar
//
//  Created by Ota-Benga Amaize on 1/20/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRChatLogs.h"
#import "RDRChatMessage.h"
#import "RDRUser.h"

NSString * const RDRChatLogDidReceiveNewMessageNotification = @"RDRChatLogDidReceiveNewMessageNotification";
NSString * const RDRChatLogMessageSenderUserInfoKey = @"RDRChatLogMessageSenderUserInfoKey";
NSString * const RDRChatLogNewMessageIndexUserInfoKey = @"RDRChatLogNewMessageIndexUserInfoKey";
NSString * const RDRChatLogMessageUserInfoKey = @"RDRChatLogMessageUserInfoKey";

@interface RDRChatLogs ()
@property (nonatomic, strong) NSMutableDictionary* chatLogs;
@end

@implementation RDRChatLogs

+ (RDRChatLogs*)sharedChatLogs
{
    static RDRChatLogs *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [RDRChatLogs new];
    });
    return instance;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.chatLogs = [NSMutableDictionary new];
    }
    return self;
}

#pragma mark - Public Methods
- (NSArray*)chatLogForUserWithPeripheralID:(NSString *)pid
{
    NSArray *log = self.chatLogs[pid];
    if (!log) {
        self.chatLogs[pid] = [NSMutableArray new];
    }
    return self.chatLogs[pid];
}

- (void)addIncomingMessage:(NSString *)message forUserWithPeripheralID:(NSString *)pid
{
    NSMutableArray *log = self.chatLogs[pid];
    if (!log) {
        log = [NSMutableArray new];
        self.chatLogs[pid] = log;
    }
    RDRChatMessage *chatMessageObject = [[RDRChatMessage alloc] initWithMessage:message sender:pid timestamp:[NSDate date]];
    [log addObject:chatMessageObject];
    
    // Notify that there is a new incoming message from user with this PID
    [[NSNotificationCenter defaultCenter] postNotificationName:RDRChatLogDidReceiveNewMessageNotification object:pid userInfo:@{RDRChatLogMessageSenderUserInfoKey:pid,RDRChatLogNewMessageIndexUserInfoKey:@((log.count-1)), RDRChatLogMessageUserInfoKey:message}];
}

- (void)addOutgoingMessage:(NSString *)message forUserWithPeripheralID:(NSString *)pid
{
    NSMutableArray *log = self.chatLogs[pid];
    if (!log) {
        log = [NSMutableArray new];
        self.chatLogs[pid] = log;
    }
    [log addObject:[[RDRChatMessage alloc] initWithMessage:message sender:[RDRUser currentUser] timestamp:[NSDate date]]];
}

@end
