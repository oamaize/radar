//
//  RDRChatLogs.h
//  Radar
//
//  Created by Ota-Benga Amaize on 1/20/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <Foundation/Foundation.h>

UIKIT_EXTERN NSString * const RDRChatLogDidReceiveNewMessageNotification;
UIKIT_EXTERN NSString * const RDRChatLogMessageSenderUserInfoKey;
UIKIT_EXTERN NSString * const RDRChatLogNewMessageIndexUserInfoKey;
UIKIT_EXTERN NSString * const RDRChatLogMessageUserInfoKey;

@interface RDRChatLogs : NSObject


+ (RDRChatLogs *) sharedChatLogs;

- (NSArray*)chatLogForUserWithPeripheralID:(NSString*)pid;
- (void)addIncomingMessage:(NSString*)message forUserWithPeripheralID:(NSString*)pid;
- (void)addOutgoingMessage:(NSString*)message forUserWithPeripheralID:(NSString*)pid;

@end
