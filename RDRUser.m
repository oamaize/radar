//
//  RDRUser.m
//  Radar
//
//  Created by Ota-Benga Amaize on 1/12/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import "RDRUser.h"
#import <Parse/Parse.h>

static RDRUser *sCurrentUser = nil;

@implementation RDRUser

- (instancetype)initWithFirstName:(NSString *)firstName lastName:(NSString *)lastName phoneNumber:(NSString *)phoneNumber imageUrlString:(NSString *)imageUrlString
{
    self = [super init];
    if (self) {
        _firstName = [firstName copy];
        _lastName = [lastName copy];
        _imageURLString = [imageUrlString copy];
        NSAssert(phoneNumber, @"phone number cannot be nil");
        _phoneNumber = [phoneNumber copy];
    }
    return self;
}

+ (void)setCurrentUser:(RDRUser *)user
{
    NSAssert(user.phoneNumber, @"user phone number must not be nil");
    sCurrentUser = user;
    
    [PFCloud callFunctionInBackground:@"getStatus" withParameters:@{@"phoneNumber": user.phoneNumber} block:^(id object, NSError *error) {
        if (!error) {
            //NSCAssert([object isKindOfClass:[NSNumber class]], @"Not a numbah");
            [[NSUserDefaults standardUserDefaults] setBool:![object[@"dnd"] boolValue] forKey:RDRDefaultsKeyDoNotDisturb];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"DND flag: %@ \n", object[@"dnd"]);
            NSLog(@"forceSync flag: %@  \n", object[@"forceSync"]);

        } else {
            NSLog(@"Get DND Status Error: %@", error);
        }
    }];
    
    NSDictionary *myInfoDict = @{@"firstName" : user.firstName,
                                 @"lastName" : user.lastName,
                                 @"phoneNumber" : user.phoneNumber,
                                 @"imageUrl" : user.imageURLString};
    
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    path = [path stringByAppendingPathComponent:@"myinfo.plist"];
    
    if(![myInfoDict writeToFile:path atomically:YES]) {
        NSLog(@"Write file failed..");
    }
}

+ (RDRUser*)currentUser
{
    return sCurrentUser;
}

+ (RDRUser*)userFromDisk
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    path = [path stringByAppendingPathComponent:@"myinfo.plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    if (!dict) {
        return nil;
    }
    
    NSAssert(dict[@"phoneNumber"], @"phone number was found to be nil on disk");
    
    RDRUser *user = [[RDRUser alloc] initWithFirstName:dict[@"firstName"] lastName:dict[@"lastName"] phoneNumber:dict[@"phoneNumber"] imageUrlString:dict[@"imageUrl"]];
    return user;
    
}

- (NSString*)fullName
{
    return [[NSString stringWithFormat:@"%@ %@", _firstName ? : @"", _lastName ? : @""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (NSDictionary *)dictionaryRepresentation
{
  return @{@"firstName" : self.firstName,
   @"lastName" : self.lastName,
   @"phoneNumber" : self.phoneNumber,
   @"picture" : self.imageURLString};
}

-(BOOL)isEqual:(id)object
{
    if (self == object) {
        return YES;
    }
    if (self.class != [object class]) {
        return NO;
    }
    RDRUser *otherUsr = (RDRUser*)object;
    if ([otherUsr.phoneNumber isEqualToString:_phoneNumber]) {
        return YES;
    }
    return NO;
}

- (NSUInteger)hash
{
    return [_phoneNumber hash];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"firstName: %@\nlastName: %@\nphoneNumber: %@\nimageUrl: %@\nimage: %@", _firstName, _lastName, _phoneNumber, _imageURLString, _image];
}

@end
