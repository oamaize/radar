//
//  RDRUser.h
//  Radar
//
//  Created by Ota-Benga Amaize on 1/12/14.
//  Copyright (c) 2014 Ota-Benga Amaize. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RDRUserGender) {
    RDRUserGenderUnknown = 0,
    RDRUserGenderMale = 1,
    RDRUserGenderFemale = 2,
};

typedef NS_ENUM(NSInteger, RDRUserType) {
    RDRLocalUserType = 0,
    RDRContactUserType = 1,
};

@interface RDRUser : NSObject

@property (nonatomic, copy, readonly) NSString *phoneNumber;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *imageURLString;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign) RDRUserGender gender;
@property (nonatomic, readonly) NSString *fullName;
@property (nonatomic, assign) RDRUserType userType;

+ (RDRUser *)currentUser;
+ (void)setCurrentUser:(RDRUser *)user;
+ (RDRUser *)userFromDisk;
- (instancetype)initWithFirstName:(NSString *)firstName lastName:(NSString*)lastName phoneNumber:(NSString *)phoneNumber imageUrlString:(NSString *)imageUrlString;
- (NSDictionary *)dictionaryRepresentation;

@end